<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_regs extends CI_Model {
    
    function get_user_regs($fb_userdata) {
        $fb_id = $fb_userdata['oauth_uid'];

        //Get user regs
        $user_regs = $this->db->query("SELECT * FROM regs 
        LEFT JOIN events ON regs.event_id = events.id
        WHERE regs.fb_id = '$fb_id'");
        return $user_regs->result(); 
    }
}