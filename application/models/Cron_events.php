<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cron_events extends CI_Model {
    
    function insert_new_events() {
        
        $next_week = date("W", strtotime("Monday this week"));
        $year = date("Y", strtotime("Monday this week"));

        //Get the week template
        $new_events_datas = $this->db->query("SELECT * FROM schedule_template ORDER BY day, time ASC");

        foreach ($new_events_datas->result() as $new_events_data) {

            $day = $new_events_data->day;
            $time = $new_events_data->time;

            $event_time = date("$year-m-d $time", strtotime("$year-W$next_week-$day"));

            $day_name = $new_events_data->day_name;
            $category = $new_events_data->category;
            $headcount = $new_events_data->headcount;
            $deadline = $new_events_data->deadline;
            $color = $new_events_data->color;

            //Event existence check
            $event_check = $this->db->query("SELECT * FROM events WHERE date = '$event_time' AND category = '$category'");
            $event_check_num = $event_check->num_rows()."<br>";
            
            if ($event_check_num == 0) {
                $this->db->query("INSERT INTO events (date, category, day, headcount, deadline, color) VALUES ('$event_time','$category', '$day_name', $headcount, '$deadline', '$color')");
            }
            
            else {
                echo "";
            }

        }

    }
}