<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_card_login extends CI_Model {

    function card_login($ticket_number) {

        $user_login_check = $this->db->query("SELECT user.* FROM user LEFT JOIN tickets ON user.ticket_type = tickets.slug WHERE ticket_number = '$ticket_number'");
        $user_login_data = $user_login_check->result();

        if ($user_login_check->num_rows() > 0) {

            $fb_id = $user_login_data[0]->fb_id;
            $ticket_type =  $user_login_data[0]->ticket_type;
            $current_occasion = $user_login_data[0]->ticket_occasion-1;
            
            return (array) $user_login_check->result();
        }
        else { 
            return false;
        }
    }
}