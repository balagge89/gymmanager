<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_events extends CI_Model {
    
    function get_home_events() {

        //GET global settings
        $settings = $this->settings->get_settings();

        //Week switcher
        if ( (date("N H:m:s") >= ($settings[0]->week_change_day.$settings[0]->week_change_time) ) ) {
            $current_week = "next";
            }
        else { 
            $current_week = "this";
        }

        $this_monday = date("Y-m-d 00:00:01", strtotime("Monday $current_week week"));
        $this_sunday = date("Y-m-d 23:59:59", strtotime("Sunday $current_week week"));

        //Get user data
        $event_datas = $this->db->query("SELECT * FROM events WHERE date BETWEEN '$this_monday' AND '$this_sunday'");
        return $event_datas->result(); 
    }

    function get_events_attendees($events_data,$fb_userdata) {
        
        $fb_id = $fb_userdata['oauth_uid'];
        
        foreach ($events_data as $event_data) { 

            $event_id = $event_data->id;

            //Get event attendees
            $event_attendees = $this->db->query("SELECT * FROM regs LEFT JOIN user ON regs.fb_id = user.fb_id  WHERE event_id = '$event_id' ORDER BY regs.date");
            $event_reg_check = $this->db->query("SELECT * FROM regs WHERE event_id = '$event_id' AND fb_id = '$fb_id' ORDER BY regs.date");

            $event_attendees_list[$event_id]['attendees_list'] = $event_attendees->result();
            $event_attendees_list[$event_id]['attendees'] = $event_attendees->num_rows();
            $event_attendees_list[$event_id]['reg_check'] = $event_reg_check->num_rows();
        }

        return $event_attendees_list;
    }
}