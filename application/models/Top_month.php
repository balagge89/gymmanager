<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Top_month extends CI_Model {

    
    function get_top_month() {

        $page = $page ?? 0;
        $month =  date("Y-m", (strtotime("- $page month")));
        //$month =  "2020-09";


        $month_top = $this->db->query("SELECT COUNT(ticket_checkin.fb_id) as checkins, ticket_checkin.fb_id, user.name FROM ticket_checkin 
							LEFT JOIN user ON ticket_checkin.fb_id = user.fb_id 
							WHERE ticket_checkin.date 
							BETWEEN '$month-01 00:00:01' AND '$month-31 23:59:59' 
							GROUP BY ticket_checkin.fb_id ORDER BY checkins desc");
        
        $month_top_list = $month_top->result();
        return $month_top_list;
    }
}