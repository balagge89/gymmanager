<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends CI_Model {
    
    function get_settings() {
        
        $settings = $this->db->query('SELECT * FROM settings');
        return $settings->result();
        
    }
}