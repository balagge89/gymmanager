<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coaches_list extends CI_Model
{
    function coaches()
    {

        $coach_list = $this->db->query("SELECT * FROM coaches");
        $coach_list_data = $coach_list->result();

        return $coach_list_data;
    }

    function update($formData)
    {
        
        $coach_id = $this->input->post('coach_id');
        $coach_name = $this->input->post('coach_name');
       
        $this->db->query("UPDATE coaches SET coach_name = '$coach_name' WHERE coach_id = '$coach_id'");
        
    }

    function add_new($formData)
    {
        $coach_name = $this->input->post('coach_name');
        
        
        $this->db->query("INSERT INTO coaches (coach_name) VALUES ('$coach_name')");
    }

    function delete($coach_id)
    {
        $this->db->query("DELETE FROM coaches WHERE coach_id = '$coach_id'");
    }
}
