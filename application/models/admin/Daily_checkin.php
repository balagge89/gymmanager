<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Daily_checkin extends CI_Model {

    function checkins($day) {

        $page = strtotime("- $day day");
		$today = date("Y-m-d", $page);
        
        $daily_checkins = $this->db->query("SELECT * FROM `ticket_checkin` LEFT JOIN user ON ticket_checkin.fb_id = user.fb_id WHERE date BETWEEN '$today 00:00:00' AND '$today 23:59:59' GROUP BY ticket_checkin.fb_id ORDER BY date DESC");
        
        $daily_checkins_data['daily_checkins_list'] = $daily_checkins->result();
        $daily_checkins_data['daily_checkins_sum'] = $daily_checkins->num_rows();

        return $daily_checkins_data;
    }

    function notcheckins($day) {

        $page = strtotime("- $day day");
		$today = date("Y-m-d", $page);
        
        $daily_notcheckins = $this->db->query("SELECT events.*, regs.fb_id, user.* FROM events LEFT JOIN regs ON events.id = regs.event_id LEFT JOIN user ON regs.fb_id = user.fb_id WHERE regs.fb_id > 0 AND events.date BETWEEN '$today 00:00:00' AND '$today 23:59:59'
        AND events.kept = 'yes' AND ticket_number NOT IN 
            (SELECT ticket_checkin.ticket_number FROM ticket_checkin LEFT JOIN user ON ticket_checkin.fb_id = user.fb_id WHERE date BETWEEN '$today 00:00:00' AND '$today 23:59:59' GROUP BY ticket_checkin.fb_id ORDER BY date DESC) ORDER BY events.date ASC");

        $daily_checkins_data['daily_checkins_list'] = $daily_notcheckins->result();
        $daily_checkins_data['daily_checkins_sum'] = $daily_notcheckins->num_rows();

        return $daily_checkins_data;
    }
}