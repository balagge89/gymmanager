<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products_list extends CI_Model
{
    function products()
    {

        $product_list = $this->db->query("SELECT * FROM products");
        $product_list_data = $product_list->result();

        return $product_list_data;
    }

    function update($formData)
    {
        
        $product_id = $this->input->post('product_id');
        $product_name = $this->input->post('product_name');
        $product_desc = $this->input->post('product_desc');
        $product_stock = $this->input->post('product_stock');
        $product_price = $this->input->post('product_price');
        $product_barcode =  $this->input->post('product_barcode');

       $this->db->query("UPDATE products SET product_name = '$product_name', product_desc = '$product_desc', product_stock = '$product_stock', product_price = '$product_price', product_barcode = '$product_barcode' WHERE id = '$product_id'");
       
       if (!empty($formData['file_name'])) {
        $product_img = $formData['file_name'];
            $this->db->query("UPDATE products SET product_img = '$product_img' WHERE id = '$product_id'");
       }
    }

    function add_new($formData)
    {
        $product_img = $formData['file_name'];
        $product_name = $this->input->post('product_name');
        $product_desc = $this->input->post('product_desc');
        $product_stock = $this->input->post('product_stock');
        $product_price = $this->input->post('product_price');
        $product_barcode =  $this->input->post('product_barcode');
        
        $this->db->query("INSERT INTO products (product_name, product_desc, product_stock, product_price, product_barcode, product_img) VALUES ('$product_name', '$product_desc', '$product_stock', '$product_price', '$product_barcode', '$product_img')");
    }

    function delete($product_id)
    {
        $this->db->query("DELETE FROM products WHERE id = '$product_id'");
    }
}
