<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tickets_list extends CI_Model
{
    public function sanitizeString($str) {
        $str = preg_replace('/[áàãâä]/ui', 'a', $str);
        $str = preg_replace('/[éèêë]/ui', 'e', $str);
        $str = preg_replace('/[íìîï]/ui', 'i', $str);
        $str = preg_replace('/[óòõôőö]/ui', 'o', $str);
        $str = preg_replace('/[úùûüű]/ui', 'u', $str);
        $str = preg_replace('/[ç]/ui', 'c', $str);
        $str = preg_replace('/[^a-z0-9]/i', '_', $str);
        $str = preg_replace('/_+/', '_', $str);
    
        return strtolower($str);
    }

    function tickets()
    {

        $ticket_list = $this->db->query("SELECT * FROM tickets");
        $ticket_list_data = $ticket_list->result();

        return $ticket_list_data;
    }

    function update()
    {
 
        $ticket_id = $this->input->post('ticket_id');
        $ticket_name = $this->input->post('ticket_name');
        $ticket_occasion = $this->input->post('ticket_occasion');
        $ticket_valid = $this->input->post('ticket_valid');
        $ticket_price = $this->input->post('ticket_price');
        $ticket_slug =  $this->sanitizeString($ticket_name);

        $this->db->query("UPDATE tickets SET ticket_name = '$ticket_name', occasion = '$ticket_occasion', day = '$ticket_valid', price = '$ticket_price', slug = '$ticket_slug' WHERE id = '$ticket_id'");
    }

    function add_new()
    {
        $ticket_name = $this->input->post('ticket_name');
        $ticket_occasion = $this->input->post('ticket_occasion');
        $ticket_valid = $this->input->post('ticket_valid');
        $ticket_slug =  $this->sanitizeString($ticket_name);
        $ticket_price = $this->input->post('ticket_price');
        
        $this->db->query("INSERT INTO tickets (ticket_name, occasion, day, price, slug) VALUES ('$ticket_name', '$ticket_occasion', '$ticket_valid', '$ticket_price', '$ticket_slug')");
    }

    function delete($ticket_id)
    {
        $this->db->query("DELETE FROM tickets WHERE id = '$ticket_id'");
    }
}
