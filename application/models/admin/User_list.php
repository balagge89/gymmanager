<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_list extends CI_Model
{

    public function fetch_data($query)
    {
        $this->db->like('name', $query);
        $query = $this->db->get("user");
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
}
