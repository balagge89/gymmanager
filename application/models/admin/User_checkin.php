<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_checkin extends CI_Model {

    function checkin($ticket_number) {

        $user_ticket_check = $this->db->query("SELECT user.* FROM user LEFT JOIN tickets ON user.ticket_type = tickets.slug WHERE ticket_number = '$ticket_number'");
        $user_ticket_data = $user_ticket_check->result();

        if ($user_ticket_check->num_rows() > 0) {

            $fb_id = $user_ticket_data[0]->fb_id;
            $ticket_type =  $user_ticket_data[0]->ticket_type;
            $current_occasion = $user_ticket_data[0]->ticket_occasion-1;
            
            $this->db->query("UPDATE user SET ticket_occasion = '$current_occasion' WHERE ticket_number = '$ticket_number'");
			$this->db->query("INSERT INTO ticket_checkin (ticket_number,fb_id, ticket_type) VALUES ('$ticket_number', '$fb_id', '$ticket_type')");
            
            return $user_ticket_check->result();
        }
        else { 
            return false;
        }
    }
}