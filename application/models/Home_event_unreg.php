<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_event_unreg extends CI_Model {

    function event_unreg($event_id,$fb_userdata) {

        $fb_id = $fb_userdata['oauth_uid'];
        $settings = $this->settings->get_settings();

        //Get event data
        $event_data = $this->db->query("SELECT * FROM events WHERE id = '$event_id'");
        $event_datas = $event_data->result();
        
        //Count event deadline
        $event_deadline = date("Y-m-d H:i:s", time() + (60*($event_datas[0]->deadline))); ;

        //Get user registration time
        $quick_unreg = $this->db->query("SELECT date FROM regs WHERE fb_id='$fb_id' AND event_id = '$event_id'");
        $quick_unreg_check = $quick_unreg->result();

        //Count quick unreg deadline
        $reg_time_diff = (strtotime(date("Y-m-d H:i:s")) - $quick_unreg_check[0]->date);

        //Declare returning results;
        $user_event_unreg = array();

        //If registration is not older than 60 sec
        if ($reg_time_diff < $settings[0]->quick_unreg_limit) {
            $this->db->query("DELETE FROM regs WHERE fb_id='$fb_id' AND event_id = '$event_id'");
            $user_event_unreg['result'] = 1;
            $user_event_unreg['message'] = $this->lang->line('event_unreg_success');
        }
        
        //If unregistration deadline is over
        elseif ($event_deadline > $event_datas[0]->date) {
            $user_event_unreg['result'] = 0;
            $user_event_unreg['message'] = $this->lang->line('event_unreg_fail_deadline');
        }
        
        else {
            $this->db->query("DELETE FROM regs WHERE fb_id='$fb_id' AND event_id = '$event_id'");
            $user_event_unreg['result'] = 1;
            $user_event_unreg['message'] = $this->lang->line('event_unreg_success');
        }
        return $user_event_unreg;
    }
}