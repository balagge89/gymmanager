<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_event_reg extends CI_Model {

    function event_reg($event_id,$fb_userdata) {
        
        $fb_id = $fb_userdata['oauth_uid'];

        $settings = $this->settings->get_settings();

        //Get event data
        $event_data = $this->db->query("SELECT * FROM events WHERE id = '$event_id'");
        $event_datas = $event_data->result();

        //Count event deadline
        $event_deadline = date("Y-m-d H:i:s", time() + (60*($event_datas[0]->deadline))); ;

        //Count event registrations
        $event_reg_nums = $this->db->query("SELECT COUNT(fb_id) as reg_nums FROM regs WHERE event_id = '$event_id'");;
        $event_regs = $event_reg_nums->result();

        //Registration check
        $user_reg_check = $this->db->query("SELECT fb_id FROM regs WHERE fb_id = '$fb_id' AND event_id = '$event_id'");
        $reg_check = $user_reg_check->num_rows();

        //Declare returning results;
        $user_event_reg = array();

        //If event is full
        if ($event_regs[0]->reg_nums >= $event_datas[0]->headcount) { 

            $user_event_reg['result'] = 0;
            $user_event_reg['message'] = $this->lang->line('event_reg_fail_full');

        }
        // If over the deadline, but more than min attendees
        elseif ( ($event_regs[0]->reg_nums < $event_datas[0]->headcount) && ($event_regs[0]->reg_nums >= $settings[0]->min_attendees) && (date("Y-m-d H:i:s") < $event_datas[0]->date)) {
            
            $this->db->query("INSERT INTO regs (fb_id, event_id) VALUES ('$fb_id', '$event_id')");
            $user_event_reg['result'] = 1;
            $user_event_reg['message'] = $this->lang->line('event_reg_success');

        }
        //If over the deadline, but not enough attendees
        elseif ( ($event_regs[0]->reg_nums < $settings[0]->min_attendees) && ($event_deadline > $event_datas[0]->date) ) {

            $user_event_reg['result'] = 0;
            $user_event_reg['message'] = $this->lang->line('event_reg_fail_deadline');

        }
        //If the user is already registered
        elseif ($reg_check > 0) {

            $user_event_reg['result'] = 0;
            $user_event_reg['message'] = $this->lang->line('event_reg_fail_regged');;

        }
    
        else {
            $this->db->query("INSERT INTO regs (fb_id, event_id) VALUES ('$fb_id', '$event_id')");

            $user_event_reg['result'] = 1;
            $user_event_reg['message'] = $this->lang->line('event_reg_success');;
            
        }
        return $user_event_reg;
    }
}