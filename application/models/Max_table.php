<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Max_table extends CI_Model {
    
    function get_max_table() {
        
        $max_table_datas = $this->db->query('SELECT user.fb_id, user.name as name, max.* FROM max left join user on user.fb_id = max.fb_id ORDER BY user.name');
        return $max_table_datas->result();
        
    }
}