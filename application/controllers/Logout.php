<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Logout extends CI_Controller { 

    function __construct() { 
        parent::__construct(); 
         
        // Load facebook oauth library 
        $this->load->library('facebook');
        $this->load->library('google');

    } 

    public function index(){
		// Reset OAuth access token
		$this->google->revokeToken();
		
		// Remove token and user data from the session
		$this->session->unset_userdata('loggedIn');
		$this->session->unset_userdata('userData');

		// Destroy entire session data
        $this->session->sess_destroy();
        // Remove local Facebook session 
		$this->facebook->destroy_session(); 
		// Redirect to login page
		redirect('user_authentication/');
    }
}