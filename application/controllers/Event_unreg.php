<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_unreg extends CI_Controller {

    public function index($event_id) {

        $fb_userdata = $this->session->userdata('userData');

        $this->load->model('Home_event_unreg');
        $event_unreg_result = $this->Home_event_unreg->event_unreg($event_id,$fb_userdata);

        $this->session->set_flashdata('event_unreg_result',$event_unreg_result);
        
        redirect("home");
        
    }
}