<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_Authentication extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        // Load facebook oauth library 
        $this->load->library('facebook');
        $this->load->library('google');

        // Load user model 
        $this->load->model('user');
    }

    public function index()
    {
        $userData = array();

        // Authenticate user with facebook 
        if ($this->facebook->is_authenticated()) {
            // Get user info from facebook 
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture');
            $access_token = $this->facebook->is_authenticated();

            // Preparing data for database insertion 
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']    = !empty($fbUser['id']) ? $fbUser['id'] : '';;
            $userData['first_name']    = !empty($fbUser['first_name']) ? $fbUser['first_name'] : '';
            $userData['last_name']    = !empty($fbUser['last_name']) ? $fbUser['last_name'] : '';
            $userData['email']        = !empty($fbUser['email']) ? $fbUser['email'] : '';
            $userData['gender']        = !empty($fbUser['gender']) ? $fbUser['gender'] : '';
            $userData['picture']    = "https://graph.facebook.com/" . $userData['oauth_uid'] . "/picture?type=square&width=800&height=800&access_token=" . $access_token;
            $userData['link']        = !empty($fbUser['link']) ? $fbUser['link'] : 'https://www.facebook.com/';

            // Insert or update user data to the database 
            $userID = $this->user->checkUser($userData);

            // Check user data insert or update status 
            if (!empty($userID)) {
                $data['userData'] = $userData;

                // Store the user profile info into session 
                $this->session->set_userdata('userData', $userData);
                return redirect('home');
            } else {
                $data['userData'] = array();
            }

            // Facebook logout URL 
            $data['logoutURL'] = $this->facebook->logout_url();
        } else {
            // Facebook authentication url 
            $data['authURL'] =  $this->facebook->login_url();
        }

        if ($this->session->userdata('loggedIn') == true) {
            redirect('home');
        }

        if (isset($_GET['code'])) {
            // Authenticate user with google
            if ($this->google->getAuthenticate()) {

                // Get user info from google
                $gpInfo = $this->google->getUserInfo();

                // Preparing data for database insertion
                $userData['oauth_provider'] = 'google';
                $userData['oauth_uid']         = $gpInfo['id'];
                $userData['first_name']     = $gpInfo['given_name'];
                $userData['last_name']         = $gpInfo['family_name'];
                $userData['email']             = $gpInfo['email'];
                $userData['gender']         = !empty($gpInfo['gender']) ? $gpInfo['gender'] : '';
                $userData['locale']         = !empty($gpInfo['locale']) ? $gpInfo['locale'] : '';
                $pic_url = explode("=s96", $gpInfo['picture']);
                $userData['picture']         = !empty($gpInfo['picture']) ? $pic_url[0] : '';

                // Insert or update user data to the database
                $userID = $this->user->checkUser($userData);

                // Store the status and user profile info into session
                $this->session->set_userdata('loggedIn', true);
                $this->session->set_userdata('userData', $userData);

                // Redirect to profile page

                redirect('home');
            }
        }
        //Card Login
        if (isset($_POST['card_number'])) {
            $ticket_number =  $this->input->post('card_number');
            $this->load->model('user_card_login');
            $data['userData'] =  $this->user_card_login->card_login($ticket_number);
            if (!empty($data['userData'])) {
                $userData = array();
            
                //Process the user data
                foreach($data['userData'] as $userdataObject){
    
                    $userData['oauth_uid'] = $userdataObject->fb_id;
                    $userData['oauth_provider'] = 'card';
                    $userData['first_name'] = $userdataObject->first_name;
                    $userData['last_name'] = $userdataObject->last_name;
                    $userData['email'] = $userdataObject->email;
                    $userData['picture'] = $userdataObject->img_url;
    
                }
                // Store the status and user profile info into session
                $this->session->set_userdata('loggedIn', true);
                $this->session->set_userdata('userData', $userData);
    
                // Redirect to profile page
                redirect('home');
            }
            elseif (empty($data['userData'])) {
                redirect('user_authentication');
            }
            
        }

        // Google authentication url
        $data['loginURL'] = $this->google->loginURL();
        // Load google login view
        //$this->load->view('user_authentication/index',$data); 

        // Load login/profile view 
        $this->load->view('user_authentication/index', $data);
    }

    public function logout()
    {
        // Reset OAuth access token
        $this->google->revokeToken();

        // Remove token and user data from the session
        $this->session->unset_userdata('loggedIn');
        $this->session->unset_userdata('userData');

        // Destroy entire session data
        $this->session->sess_destroy();
        // Remove local Facebook session 
        $this->facebook->destroy_session();
        // Redirect to login page
        redirect('user_authentication/');
    }
}
