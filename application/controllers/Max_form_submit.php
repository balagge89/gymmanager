<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Max_form_submit extends CI_Controller {

    public function index() {

        $fb_userdata = $this->session->userdata('userData');

        $this->load->model('User_max');
        $this->User_max->insert_user_max($fb_userdata);
        $this->session->set_flashdata('max_edit_success',1);
        redirect("home");
        
    }
}