<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event_reg extends CI_Controller {

    public function index($event_id) {

        $fb_userdata = $this->session->userdata('userData');

        $this->load->model('Home_event_reg');
        $event_reg_result = $this->Home_event_reg->event_reg($event_id,$fb_userdata);

        $this->session->set_flashdata('event_reg_result',$event_reg_result);
        
        redirect("home");
        
    }
}