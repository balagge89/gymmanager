<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin/user_list');
        if ($this->session->userdata('admin')) {
        } else {
            redirect('admin/login');
        }
    }

    public function index()
    {
        $data['admin_page_title'] = $this->lang->line('admin_users_title');
        $this->load->view('admin/user_list_data', $data);
    }

    public function fetch_user()
    {
        $query = '';
        if ($this->input->post('query')) {
            $query = $this->input->post('query');
            $data['user_data'] = $this->user_list->fetch_data($query);
            // process data to $output
            //print_r($data['user_data']);
            $this->load->view('admin/user_list', $data);
        }
    }
}
