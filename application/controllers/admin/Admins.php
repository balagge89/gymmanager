<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admins extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('admin')) {

            $this->load->model('admin/admins_list');
            $data['admin_list_data'] =  $this->admins_list->admins();
            $data['admin_page_title'] = $this->lang->line('admin_admins_title');
            $this->load->view('admin/admin_list_data', $data);
        } else {
            redirect('admin/login');
        }
    }

    public function update()
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));

            $this->load->model('admin/admins_list');
            $this->admins_list->update($formData);
            $this->session->set_flashdata('admin_edit_success', 1);
            redirect("admin/admins");
        } else {
            redirect('admin/login');
        }
    }

    public function add_new()
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));

            // Pass $formData to the model
            $this->load->model('admin/admins_list');
            $this->admins_list->add_new($formData);
            $this->session->set_flashdata('admin_add_new_success', 1);
            redirect("admin/admins");
        } else {
            redirect('admin/login');
        }
    }

    public function delete($admin_id)
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));
            $this->load->model('admin/admins_list');
            $this->admins_list->delete($admin_id);
            $this->session->set_flashdata('admin_delete_success', 1);
            redirect("admin/admins");
        } else {
            redirect('admin/login');
        }
    }
}
