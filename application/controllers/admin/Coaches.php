<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Coaches extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('admin')) {

            $this->load->model('admin/coaches_list');
            $data['coach_list_data'] =  $this->coaches_list->coaches();
            $data['admin_page_title'] = $this->lang->line('admin_coaches_title');
            $this->load->view('admin/coach_list_data', $data);
        } else {
            redirect('admin/login');
        }
    }

    public function update()
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));

            $this->load->model('admin/coaches_list');
            $this->coaches_list->update($formData);
            $this->session->set_flashdata('coach_edit_success', 1);
            redirect("admin/coaches");
        } else {
            redirect('admin/login');
        }
    }

    public function add_new()
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));

            // Pass $formData to the model
            $this->load->model('admin/coaches_list');
            $this->coaches_list->add_new($formData);
            $this->session->set_flashdata('coach_add_new_success', 1);
            redirect("admin/coaches");
        } else {
            redirect('admin/login');
        }
    }

    public function delete($coach_id)
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));
            $this->load->model('admin/coaches_list');
            $this->coaches_list->delete($coach_id);
            $this->session->set_flashdata('coach_delete_success', 1);
            redirect("admin/coaches");
        } else {
            redirect('admin/login');
        }
    }
}
