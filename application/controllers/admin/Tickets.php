<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tickets extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('admin')) {

            $this->load->model('admin/tickets_list');
            $data['ticket_list_data'] =  $this->tickets_list->tickets();
            $data['admin_page_title'] = $this->lang->line('admin_tickets_title');
            $this->load->view('admin/ticket_list_data', $data);
        } else {
            redirect('admin/login'); 
        }
    }

    public function update()
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));
            $this->load->model('admin/tickets_list');
            $this->tickets_list->update();
            $this->session->set_flashdata('ticket_edit_success',1);
            redirect("admin/tickets");
           
        } else {
            redirect('admin/login'); 
        }
    }

    public function add_new()
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));
            $this->load->model('admin/tickets_list');
            $this->tickets_list->add_new();
            $this->session->set_flashdata('ticket_add_new_success',1);
            redirect("admin/tickets");
           
        } else {
            redirect('admin/login'); 
        }
    }

    public function delete($ticket_id)
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));
            $this->load->model('admin/tickets_list');
            $this->tickets_list->delete($ticket_id);
            $this->session->set_flashdata('ticket_delete_success',1);
            redirect("admin/tickets");
           
        } else {
            redirect('admin/login'); 
        }
    }

}
