<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Checkin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $data['admin_page_title'] = "Beléptetés";
        $this->load->view('admin/checkin_form',$data);
    }

    public function index()
    {
        $ticket_number = $this->input->post('ticket_number');
    
        if (isset($ticket_number)) {
            $this->load->model('admin/user_checkin');
            $data['checkin_data'] =  $this->user_checkin->checkin($ticket_number);
           
            $this->load->view('admin/checkin_data', $data);
        }
        $this->load->view('admin/footer');
    }
}
