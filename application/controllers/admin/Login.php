<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function index()
    {

        if ($this->session->userdata('admin')) {
            //declaring session  
            redirect("admin");
        } else {

            $data['admin_page_title'] =  "Bejelentkezés";
            $this->load->view('admin/login', $data);
        }
    }

    public function process()
    {
        $user = $this->input->post('user');
        $pass = md5($this->input->post('pass'));

        //Load User Data
        $this->load->model('admin/admin_login');

        $data['login_data'] =  $this->admin_login->admin_login_check($user, $pass);

        if ($data['login_data'] > 0) {
            //declaring session  
            $this->session->set_userdata(array('admin' => $user));
            redirect("admin/login");
        }
    }
}
