<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Daily extends CI_Controller
{

    public function checkins($day = 0)
    {
        if ($this->session->userdata('admin')) {

            $this->load->model('admin/daily_checkin');
            $data['daily_checkin_data'] =  $this->daily_checkin->checkins($day);
            $data['admin_page_title'] = $this->lang->line('admin_daily_checkin');
            $this->load->view('admin/daily_checkin_data', $data);
        } else {
            redirect('admin/login');
        }
    }

    public function notcheckins($day = 0)
    {
        if ($this->session->userdata('admin')) {

            $this->load->model('admin/daily_checkin');
            $data['daily_checkin_data'] =  $this->daily_checkin->notcheckins($day);
            $data['admin_page_title'] = $this->lang->line('admin_daily_notcheckin');
            $this->load->view('admin/daily_checkin_data', $data);
        } else {
            redirect('admin/login');
        }
    }
}
