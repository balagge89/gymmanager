<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Products extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('admin')) {

            $this->load->model('admin/products_list');
            $data['product_list_data'] =  $this->products_list->products();
            $data['admin_page_title'] = $this->lang->line('admin_products_title');
            $this->load->view('admin/product_list_data', $data);
        } else {
            redirect('admin/login');
        }
    }
    public function file_upload()
    {
            
            $config['upload_path'] = './assets/admin/img/products/'; // Specify the upload path
            $config['allowed_types'] = 'gif|jpg|png'; // Allowed types
            $config['max_size'] = 1024; // Maximum file size in KB
            $config['encrypt_name'] = TRUE;
            $this->upload->initialize($config);

            if (!$this->upload->do_upload('product_img')) {
                $error = array('error' => $this->upload->display_errors());
                var_dump($error);
                // Handle the error scenario
            } else {
                
                $fileData = $this->upload->data();
                $formData = array(
                    'file_name' => $fileData['file_name']
                );
                return $formData;
            }
        
    }
    public function update()
    {
        if ($this->session->userdata('admin')) {
            $this->load->library('upload');
            $this->load->helper(array('form', 'url'));

            $formData = $this->file_upload();
            $this->load->model('admin/products_list');
            $this->products_list->update($formData);
            $this->session->set_flashdata('product_edit_success', 1);
            redirect("admin/products");
        } else {
            redirect('admin/login');
        }
    }

    public function add_new()
    {
        if ($this->session->userdata('admin')) {
            $this->load->library('upload');
            $this->load->helper(array('form', 'url'));
            $formData = $this->file_upload();

            // Pass $formData to the model
            $this->load->model('admin/products_list');
            $this->products_list->add_new($formData);
            $this->session->set_flashdata('product_add_new_success', 1);
            redirect("admin/products");
        } else {
            redirect('admin/login');
        }
    }

    public function delete($products_id)
    {
        if ($this->session->userdata('admin')) {
            $this->load->helper(array('form', 'url'));
            $this->load->model('admin/products_list');
            $this->products_list->delete($products_id);
            $this->session->set_flashdata('products_delete_success', 1);
            redirect("admin/products");
        } else {
            redirect('admin/login');
        }
    }
}
