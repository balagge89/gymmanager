<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Logout extends CI_Controller
{
public function index()  
    {  
        //removing session  
        $this->session->unset_userdata('admin');  
        redirect("admin/login");  
    }  
}