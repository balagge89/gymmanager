<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    public function index() {
        if($this->session->userdata('userData')) {
            $fb_userdata = $this->session->userdata('userData');

            //Load Events Data
            $this->load->model('home_events');
            $data['events_data'] =  $this->home_events->get_home_events($fb_userdata);
            $data['events_attendees_list'] =  $this->home_events->get_events_attendees($data['events_data'],$fb_userdata);
            
            //Load User Data
            $this->load->model('user_data');
            $data['user_data'] =  $this->user_data->get_user_data($fb_userdata);
            
            // Generate QR Code
            $this->load->library('ciqrcode');        
            foreach($data['user_data'] as $userdata){
                $params['data'] = $userdata->ticket_number;
                $params['level'] = 'H'; // This refers to the error correction level
                $params['size'] = 15;
                $params['savename'] = FCPATH.'assets/front/img/qr-code//'.$userdata->ticket_number.'.png'; // FCPATH is a CodeIgniter constant that refers to the absolute path to the front controller (index.php)
                $this->ciqrcode->generate($params);
            }
            
            //Load User Max
            $this->load->model('user_max');
            $data['user_maxes'] =  $this->user_max->get_user_max($fb_userdata);

            //Load User Events
            $this->load->model('user_regs');
            $data['user_regs'] =  $this->user_regs->get_user_regs($fb_userdata);
            $data['count_user_regs'] = count($data['user_regs']);

            //Max Table data
            $this->load->model('max_table');
            $data['max_table'] =  $this->max_table->get_max_table();

            //Load Monthly Top list
            $this->load->model('top_month');
            $data['top_month'] =  $this->top_month->get_top_month();

            //Settings data
            $data['settings'] = $this->settings->get_settings();

            $this->load->view('home', $data);

        }
        else {
            return redirect('user_authentication');
        }
    }
}