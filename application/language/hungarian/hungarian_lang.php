<?php

/* HEADER MENU */
$lang['menu_dates'] = "Időpontok";
$lang['menu_profile'] = "Profilom";
$lang['menu_max_table'] = "Max Táblázat";
$lang['menu_top_month'] = "Havi Top Lista";
$lang['menu_logout'] = "Kijelenetkezés";

/* LOGIN */
$lang['login_with_facebook'] = "Bejelentkezés Facebookkal";
$lang['login_with_google'] = " Bejelentkezés Google-el ";
$lang['login_with_card'] = " Bejelentkezés Kártyaszámmal ";
$lang['card_number'] = "Kártyaszám";
$lang['login_button'] = "Belépés";

/* DATE */
$lang['date_attendees'] = "Jelentkezők";
$lang['date_signup'] = "Jelentkezem";
$lang['date_cancel'] = "Mégsem";
$lang['date_full'] = "Betelt";

/* PROFILE */
$lang['profile_data'] = "Adataim";
$lang['profile_ticket_number'] = "Bérletszám";
$lang['profile_ticket_occasion'] = "Hátralévő alkalmak";
$lang['profile_ticket_valid'] = "Bérlet érvényesség";
$lang['profile_ticket_type'] = "Bérlet típus";
$lang['profile_ticket_payment'] = "Utolsó Befizetésem";

$lang['profile_my_max'] = "Maxaim";
$lang['profile_upload_max'] = "Töltsd fel a maxaid";
$lang['profile_edit_max'] = "Szerkesztés";
$lang['profile_save_max'] = "Mentés";

$lang['profile_last_events'] = "Eddigi edzéseim";
$lang['profile_day'] = "Nap";
$lang['profile_type'] = "Típus";
$lang['profile_date'] = "Időpont";

/* MAX TABLE */
$lang['max_table_name'] = "Név";

/* NOTIFCATIONS */
$lang['max_table_success_modify'] = "Sikeres Módosítás!";
$lang['event_reg_success'] = "Sikeres Jelentkezés!";
$lang['event_reg_fail_full'] = "Az időpont betelt!";
$lang['event_reg_fail_deadline'] = "A jelentkezési határidő lejárt!";
$lang['event_reg_fail_regged'] = "Már jelentkeztél!";

$lang['event_unreg_success'] = "Sikeres Lejenlentkezés";
$lang['event_unreg_fail_deadline'] = "A lejelentkezési határidő lejárt!";


/* ADMIN - LOGIN */
$lang['admin_login_subtitle'] = "Add meg a felhasználóneved és jelszavad a belépéshez!";
$lang['admin_login_username'] = "Felhasználónév";
$lang['admin_login_password'] = "Jelszó";
$lang['admin_login_button'] = "Bejelentkezés";

/* ADMIN - NAV */
$lang['admin_nav_dashboard'] = "Vezérlőpult";
$lang['admin_nav_tickets'] = "Bérletek";
$lang['admin_nav_users'] = "Vendégek";
$lang['admin_nav_products'] = "Termékek";
$lang['admin_nav_session_sum'] = "Óra összesítő";
$lang['admin_nav_stats'] = "Statisztikák";
$lang['admin_nav_schedule'] = "Órarend";
$lang['admin_nav_coaches'] = "Edzők";
$lang['admin_nav_admins'] = "Adminok";
$lang['admin_nav_settings'] = "Beállítások";
$lang['admin_nav_logout'] = "Kijelentkezés";

/* ADMIN - CHECKIN */
$lang['admin_checkin_ticket_number'] = "Bérletszám";
$lang['admin_checkin_button'] = "Beléptetés";
$lang['admin_checkin_ticket_type'] = "Bérlet típusa";
$lang['admin_checkin_ticket_valid'] = "Érvényesség";
$lang['admin_checkin_ticket_occasion'] = "Hátralévő alkalmak";
$lang['admin_checkin_ticket_payment'] = "Befizetés dátuma";
$lang['admin_checkin_ticket_renew'] = "Bérlet megújítása";
$lang['admin_checkin_ticket_wrong_number'] = "Nem található ilyen bérlet. Kérlek húzd le újra a kártyád!";
$lang['admin_daily_checkin'] = "Napi belépők";
$lang['admin_daily_notcheckin'] = "Akik még nem léptek be";


/* ADMIN - TICKETS */
$lang['admin_tickets_title'] = "Bérletek";
$lang['admin_tickets_add_new'] = "Új hozzáadása";
$lang['admin_tickets_id'] = "Azonosító";
$lang['admin_tickets_name'] = "Bérlet neve";
$lang['admin_tickets_occasion'] = "Bérlet Alkalmak";
$lang['admin_tickets_valid'] = "Bérlet Érvényesség (napok)";
$lang['admin_tickets_price'] = "Bérlet Ár";
$lang['admin_tickets_modify'] = "Módosítás";
$lang['admin_tickets_add'] = "Hozzáadás";
$lang['admin_tickets_delete'] = "Törlés";
$lang['admin_tickets_success_update'] = "Sikeres bérlet módosítás";
$lang['admin_tickets_success_add'] = "Sikeres bérlet hozzáadás";
$lang['admin_tickets_success_delete'] = "Sikeres bérlet törlés";
$lang['admin_tickets_delete_confirm'] = "Biztosan törlöd?";


/* ADMIN - PRODUCTS */
$lang['admin_products_title'] = "Termékek";
$lang['admin_products_add_new'] = "Új hozzáadása";
$lang['admin_products_id'] = "Azonosító";
$lang['admin_products_name'] = "Termék neve";
$lang['admin_products_img'] = "Termék kép";
$lang['admin_products_desc'] = "Termék leírása";
$lang['admin_products_stock'] = "Raktárkészlet";
$lang['admin_products_price'] = "Termék Ár";
$lang['admin_products_barcode'] = "Termék vonalkód";
$lang['admin_products_modify'] = "Módosítás";
$lang['admin_products_add'] = "Hozzáadás";
$lang['admin_products_delete'] = "Törlés";
$lang['admin_products_success_update'] = "Sikeres termék módosítás";
$lang['admin_products_success_add'] = "Sikeres termék hozzáadás";
$lang['admin_products_success_delete'] = "Sikeres termék törlés";
$lang['admin_products_delete_confirm'] = "Biztosan törlöd?";


/* ADMIN - USERS */
$lang['admin_users_title'] = "Vendégek";
$lang['admin_users_search'] = "Keresés...";
$lang['admin_users_search_for'] = "Vendég keresése";
$lang['admin_users_id'] = "Azonosító";
$lang['admin_users_img'] = "Kép";
$lang['admin_users_name'] = "Név";
$lang['admin_users_email'] = "Email";
$lang['admin_users_phone_number'] = "Telefonszám";
$lang['admin_users_ticket_number'] = "Bérlet szám";
$lang['admin_users_ticket_type'] = "Bérlet típus";
$lang['admin_users_ticket_valid'] = "Bérlet érvényesség";
$lang['admin_users_ticket_occasion'] = "Bérlet alkalmak";



/* ADMIN - NOTIFICATION MESSAGES */
$lang['admin_no_search_result'] = "Nincs találat";

/* ADMIN - ADMINS */
$lang['admin_admins_title'] = "Admin felhasználók";
$lang['admin_admins_delete_confirm'] = "Biztosan törlöd?";
$lang['admin_admins_id'] = "Azonosító";
$lang['admin_admins_name'] = "Megjelenített név";
$lang['admin_admins_user'] = "Felhasználónév";
$lang['admin_admins_password'] = "Jelszó";
$lang['admin_admins_password_confirm'] = "Jelszó megerősítése";
$lang['admin_admins_modify'] = "Módosítás";
$lang['admin_admins_delete'] = "Törlés";
$lang['admin_admins_add'] = "Hozzáadás";
$lang['admin_admins_delete_confirm'] = "Biztosan törlöd?";
$lang['admin_admins_password_match'] = "Jelszó megegyezik";
$lang['admin_admins_password_not_match'] = "Jelszó nem egyezik!";
$lang['admin_admins_success_update'] = "Sikeres admin módosítás";
$lang['admin_admins_success_add'] = "Sikeres admin hozzáadás";
$lang['admin_admins_success_delete'] = "Sikeres admin törlés";


/* ADMIN - COACHES */
$lang['admin_coaches_title'] = "Edzők";
$lang['admin_coaches_id'] = "Azonosító";
$lang['admin_coaches_name'] = "Edző neve";
$lang['admin_coaches_modify'] = "Módosítás";
$lang['admin_coaches_add'] = "Hozzáadás";
$lang['admin_coaches_delete_confirm'] = "Biztosan törlöd?";
$lang['admin_coaches_success_update'] = "Sikeres edző módosítás";
$lang['admin_coaches_success_add'] = "Sikeres edző hozzáadás";
$lang['admin_coaches_success_delete'] = "Sikeres edző törlés";
?>