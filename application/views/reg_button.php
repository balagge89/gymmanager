<?php


if (!is_numeric($reg_check)) {

    //If the event is full
    if ($events_attendees_list[$event_data->id]['attendees'] > $event_data->headcount - 1) { ?>
        <button type="submit" class="btn btn-secondary application"><?= $this->lang->line('date_full'); ?><div class="ripple-container"></div></button>
    <?php }

    //If bigger than min attendees but not started
    elseif (($events_attendees_list[$event_data->id]['attendees'] < ($event_data->headcount)) && ($events_attendees_list[$event_data->id]['attendees'] > $min_attendees - 1) && (date("Y-m-d H:i:s") < $event_data->date)) { ?>
        <a href="/event_reg/index/<?= $event_data->id; ?>">
            <button type="submit" class="btn btn-primary application"><?= $this->lang->line('date_signup'); ?><div class="ripple-container"></div></button>
        </a>
    <?php }

    //If smaller than min attendes and registration dealine is passed 
    elseif ($reg_deadline > $event_data->date) { ?>
        <button type="submit" class="btn btn-secondary application"><?= $this->lang->line('date_signup'); ?><div class="ripple-container"></div></button>
    <?php } else { ?>
        <a href="/event_reg/index/<?= $event_data->id; ?>">
            <button type="submit" class="btn btn-primary application"><?= $this->lang->line('date_signup'); ?><div class="ripple-container"></div></button>
        </a>
    <?php   }
} elseif (is_numeric($reg_check)) {

    // 1 Minute Reg Check
    $reg_time = $events_attendees_list[$event_data->id]['attendees_list'][$reg_check]->date;
    $reg_time_diff = strtotime(date("Y-m-d H:i:s")) - strtotime($reg_time);

    if ($reg_time_diff < $settings[0]->quick_unreg_limit) { ?>
    <a href="/event_unreg/index/<?= $event_data->id; ?>">
        <button type="submit" class="btn btn-danger application"><?= $this->lang->line('date_cancel'); ?><div class="ripple-container"></div></button>
    </a>
    <?php }

    //If registration dealine is passed 
    elseif ($un_reg_deadline > $event_data->date) { ?>
        <button type="submit" class="btn btn-secondary application"><?= $this->lang->line('date_cancel'); ?><div class="ripple-container"></div></button>
    <?php } else { ?>
            <a href="/event_unreg/index/<?= $event_data->id; ?>">
                <button type="submit" class="btn btn-danger application"><?= $this->lang->line('date_cancel'); ?><div class="ripple-container"></div></button>
            </a>
<?php }
} ?>