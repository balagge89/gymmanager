<?php
defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php if (!empty($user_data)) { ?>
<div class="table-responsive">
    <table class="table align-middle mb-0">
        <thead class="table-light">
            <tr>
                <th><?= $this->lang->line('admin_users_id'); ?></th>
                <th><?= $this->lang->line('admin_users_img'); ?></th>
                <th><?= $this->lang->line('admin_users_name'); ?></th>
                <th><?= $this->lang->line('admin_users_email'); ?></th>
                <th><?= $this->lang->line('admin_users_phone_number'); ?></th>
                <th><?= $this->lang->line('admin_users_ticket_number'); ?></th>
                <th><?= $this->lang->line('admin_users_ticket_type'); ?></th>
                <th><?= $this->lang->line('admin_users_ticket_valid'); ?></th>
                <th><?= $this->lang->line('admin_users_ticket_occasion'); ?></th>

            </tr>
        </thead>
        <tbody>
            <?php
            
                foreach ($user_data as $user) { ?>
                    <tr>
                        <?php

                        if (!empty($user->img_url)) {
                        } else {
                            $user->img_url = base_url('assets/admin/img/profile-placeholder.jpg'); ?>
                        <?php    } ?>

                        <td><?= $user->id ?></td>
                        <td><img src="<?= $user->img_url ?>" style="width:50px;"></td>
                        <td><a href="<?= site_url('/admin/profile/') . $user->id ?>"><?= $user->name ?></a></td>
                        <td><a href="mailto:<?= $user->email ?>"><?= $user->email ?></a></td>
                        <td><a href="tel:<?= $user->phone ?>"><?= $user->phone ?></a></td>
                        <td><?= $user->ticket_number ?></td>
                        <td><?= $user->ticket_type ?></td>
                        <td><?= $user->ticket_valid ?></td>
                        <td><?= $user->ticket_occasion ?></td>
                    </tr>

            <?php } ?>

        </tbody>
    </table>
    <?php }
        elseif (empty($user_data)) {
            echo $this->lang->line('admin_no_search_result');
        }
    ?>

    </body>

    </html>