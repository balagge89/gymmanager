<!--end switcher-->
<!-- Bootstrap JS -->
<script src="<?php echo base_url('assets/admin/js/bootstrap.bundle.min.js'); ?>"></script>
<!--plugins-->

<script src="<?php echo base_url('assets/admin/plugins/simplebar/js/simplebar.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/metismenu/js/metisMenu.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/perfect-scrollbar/js/perfect-scrollbar.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/vectormap/jquery-jvectormap-2.0.2.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/vectormap/jquery-jvectormap-world-mill-en.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/apexcharts-bundle/js/apexcharts.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/chartjs/js/Chart.min.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/plugins/chartjs/js/Chart.extension.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/js/index.js'); ?>"></script>
<script src="<?php echo base_url('assets/admin/js/custom.js'); ?>"></script>
<!--app JS-->
<script src="<?php echo base_url('assets/admin/js/app.js'); ?>"></script>

</body>

</html>