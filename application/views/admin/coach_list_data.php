<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('admin/header'); ?>

<!--wrapper-->

<div class="page-content d-flex  justify-content-center my-5 my-lg-0">



    <div class="container-fluid">

        <div class="row row-cols-10 row-cols-lg-10 row-cols-xl-10">
            <div class="col mx-auto">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <h3 class="mb-4 text-center"><?= $admin_page_title ?></h3>
                            </div>
                            <div class="ms-auto">
                                <a href="javascript:;" class="btn btn-primary radius-30 mt-2 mt-lg-0" data-bs-toggle="modal" data-bs-target="#addNewcoachModal">
                                    <i class="bx bxs-plus-square"></i><?= $this->lang->line('admin_products_add_new'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-middle mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th><?= $this->lang->line('admin_coaches_id'); ?></th>
                                        <th><?= $this->lang->line('admin_coaches_name'); ?></th>
                                        <th><?= $this->lang->line('admin_admins_modify'); ?></th>
                                        <th><?= $this->lang->line('admin_admins_delete'); ?></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($coach_list_data as $coach) { ?>
                                        <tr>
                                            <td><?= $coach->coach_id ?></a></td>
                                            <td><?= $coach->coach_name ?></td>
                                            <td><a href="#" class="" data-bs-toggle="modal" data-bs-target="#coachModal<?= $coach->coach_id ?>"><i class="bx bxs-edit"></i></a></td>
                                            <td><a href="/admin/coaches/delete/<?= $coach->coach_id ?>" class="" onclick="return confirm('<?= $this->lang->line('admin_coaches_delete_confirm'); ?>');"><i class="bx bxs-trash"></i></a></td>
                                        </tr>
                                        <div class="modal fade" id="coachModal<?= $coach->coach_id ?>" tabindex="-1" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"><i class="bx bx-user-check"></i> <?= $coach->coach_name ?></h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <?= validation_errors(); ?>

                                                    <?= form_open_multipart('admin/coaches/update'); ?>
                                                    <div class="modal-body">

                                                        <input class="form-control mb-3" type="text" name="coach_id" value="<?= $coach->coach_id ?>" hidden>

                                                        <?= $this->lang->line('admin_coaches_name'); ?>
                                                        <input class="form-control mb-3" type="text" name="coach_name" value="<?= $coach->coach_name ?>" placeholder="<?= $this->lang->line('admin_coaches_name'); ?>" aria-label="default input example">

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary"><?= $this->lang->line('admin_coaches_modify'); ?></button>
                                                    </div>
                                                    <?= form_close(); ?>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>

                                </tbody>
                            </table>
                            <div class="modal fade" id="addNewcoachModal" tabindex="-1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"><i class="bx bx-user-plus"></i> <?= $this->lang->line('admin_products_add_new'); ?></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <?= validation_errors(); ?>

                                        <?= form_open_multipart('admin/coaches/add_new'); ?>
                                        <div class="modal-body">

                                            <?= $this->lang->line('admin_coaches_name'); ?>
                                            <input class="form-control mb-3" type="text" name="coach_name" placeholder="<?= $this->lang->line('admin_coaches_name'); ?>" aria-label="default input example">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary"><?= $this->lang->line('admin_coaches_add'); ?></button>
                                        </div>
                                        <?= form_close(); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>
</div>
<?php $this->load->view('admin/footer'); ?>
</body>

</html>