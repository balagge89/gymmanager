<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Gym Manager - Admin</title>

  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/front/img/favicon.svg'); ?>">
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/front/img/favicon.svg'); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />


  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

  <!--plugins-->
  <script src="<?php echo base_url('assets/admin/js/jquery.min.js'); ?>"></script>
  <link href="<?php echo base_url('assets/admin/plugins/vectormap/jquery-jvectormap-2.0.2.css'); ?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/admin/plugins/simplebar/css/simplebar.css'); ?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/admin/plugins/perfect-scrollbar/css/perfect-scrollbar.css'); ?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/admin/plugins/metismenu/css/metisMenu.min.css'); ?>" rel="stylesheet" />
  <link href="<?php echo base_url('assets/admin/plugins/notifications/css/lobibox.min.css'); ?>" rel="stylesheet" />
  
  <!-- loader-->
  <link href="<?php echo base_url('assets/admin/css/pace.min.css'); ?>" rel="stylesheet" />
  <script src="assets/admin/js/pace.min.js"></script>
  <!-- Bootstrap CSS -->
  <link href="<?php echo base_url('assets/admin/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/admin/css/bootstrap-extended.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/admin/css/custom.css'); ?>" rel="stylesheet">

  <link href="<?php echo base_url('assets/admin/css/app.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/admin/css/icons.css'); ?>" rel="stylesheet">
  <!-- Theme Style CSS -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/dark-theme.css'); ?>" />
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/semi-dark.css'); ?>" />
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/css/header-colors.css'); ?>" />

  <!--notification js -->
  <script src="<?php echo base_url('assets/admin//plugins/notifications/js/lobibox.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/admin//plugins/notifications/js/notifications.min.js'); ?>"></script>
  <script src="<?php echo base_url('assets/admin//plugins/notifications/js/notification-custom-script.js'); ?>"></script>
</head>

<body>
  <div class="page-wrapper">
    <?php if ($this->session->userdata('admin')) {
      $this->load->view('admin/nav');
      $this->load->view('admin/notifications');
    }
    ?>