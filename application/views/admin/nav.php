<div class="topbar d-flex align-items-center">
  <nav class="navbar navbar-expand">
    <div class="mobile-toggle-menu"><i class="bx bx-menu"></i></div>
  </nav>
</div>

<div class="nav-container primary-menu">
  <div class="mobile-topbar-header">
    <div>
      <img src="assets/images/logo-icon.png" class="logo-icon" alt="logo icon">
    </div>
    <div>
      <h4 class="logo-text">Rukada</h4>
    </div>
    <div class="toggle-icon ms-auto"><i class="bx bx-arrow-to-left"></i>
    </div>
  </div>
  <i class="bx bx-x mobile-menu-close"></i>
  <nav class="navbar navbar-expand-xl w-100">
    <ul class="navbar-nav justify-content-start flex-grow-1 gap-1">
      <li class="nav-item dropdown">
        <a href="<?= site_url('admin'); ?>" class="nav-link dropdown-toggle dropdown-toggle-nocaret">
          <div class="parent-icon"><i class="bx bx-home-circle"></i>
          </div>
          <div class="menu-title"><?= $this->lang->line('admin_nav_dashboard'); ?></div>
        </a>
      </li>
      <li class="nav-item dropdown">
        <a href="<?= site_url('admin/checkin'); ?>" class="nav-link dropdown-toggle dropdown-toggle-nocaret">
          <div class="parent-icon"><i class="bx bx-log-in-circle"></i>
          </div>
          <div class="menu-title"><?= $this->lang->line('admin_checkin_button'); ?></div>
        </a>
        <ul class="dropdown-menu">
          <li> <a class="dropdown-item" href="<?= site_url('admin/daily/checkins'); ?>"><i class="bx bx-right-arrow-alt"></i><?= $this->lang->line('admin_daily_checkin'); ?></a>
          </li>
          <li> <a class="dropdown-item" href="<?= site_url('admin/daily/notcheckins'); ?>"><i class="bx bx-right-arrow-alt"></i><?= $this->lang->line('admin_daily_notcheckin'); ?></a>
          </li>
      </li>
    </ul>
    </li>
    <li class="nav-item dropdown">
      <a href="<?= site_url('admin/tickets'); ?>" class="nav-link dropdown-toggle dropdown-toggle-nocaret">
        <div class="parent-icon"><i class="bx bx-id-card"></i>
        </div>
        <div class="menu-title"><?= $this->lang->line('admin_nav_tickets'); ?></div>
      </a>
    </li>

    <li class="nav-item dropdown">
      <a href="<?= site_url('admin/users'); ?>" class="nav-link dropdown-toggle dropdown-toggle-nocaret">
        <div class="parent-icon"><i class="bx bx-user"></i>
        </div>
        <div class="menu-title"><?= $this->lang->line('admin_nav_users'); ?></div>
      </a>
    </li>

    <li class="nav-item dropdown">
      <a href="<?= site_url('admin/products'); ?>" class="nav-link dropdown-toggle dropdown-toggle-nocaret">
        <div class="parent-icon"><i class="bx bx-cart"></i>
        </div>
        <div class="menu-title"><?= $this->lang->line('admin_nav_products'); ?></div>
      </a>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="javascript:;" data-bs-toggle="dropdown">
        <div class="parent-icon"><i class="bx bx-pie-chart-alt-2"></i>
        </div>
        <div class="menu-title"><?= $this->lang->line('admin_nav_session_sum'); ?></div>
      </a>

    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="javascript:;" data-bs-toggle="dropdown">
        <div class="parent-icon"><i class="bx bx-bar-chart-alt"></i>
        </div>
        <div class="menu-title"><?= $this->lang->line('admin_nav_stats'); ?></div>
      </a>
      <ul class="dropdown-menu">
        <li> <a class="dropdown-item" href="authentication-signin.html" target="_blank"><i class="bx bx-right-arrow-alt"></i>Sign In</a>
        </li>
        <li> <a class="dropdown-item" href="authentication-signup.html" target="_blank"><i class="bx bx-right-arrow-alt"></i>Sign Up</a>
        </li>
    
    </ul>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="javascript:;" data-bs-toggle="dropdown">
        <div class="parent-icon"><i class="bx bx-calendar"></i>
        </div>
        <div class="menu-title"><?= $this->lang->line('admin_nav_schedule'); ?></div>
      </a>
      <ul class="dropdown-menu">
        <li> <a class="dropdown-item" href="<?= site_url('admin/coaches'); ?>"><i class="bx bx-user-circle"></i><?= $this->lang->line('admin_nav_coaches'); ?></a>
        </li>
    </ul>
    </li>

    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="<?= site_url('admin/admins'); ?>">
        <div class="parent-icon"><i class="bx bx-lock"></i>
        </div>
        <div class="menu-title"><?= $this->lang->line('admin_nav_admins'); ?></div>
      </a>

    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="javascript:;" data-bs-toggle="dropdown">
        <div class="parent-icon"><i class="bx bx-cog"></i>
        </div>
        <div class="menu-title"><?= $this->lang->line('admin_nav_settings'); ?></div>
      </a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="<?= site_url('admin/logout'); ?>">
        <div class="parent-icon"><i class="bx bx-exit"></i>
        </div>
        <div class="menu-title"><?= $this->lang->line('admin_nav_logout'); ?></div>
      </a>
    </li>
    </ul>
  </nav>
</div>
<div class="page-breadcrumb d-none d-sm-flex align-items-center mb-3">
  <div class="breadcrumb-title pe-3">Gym Manager</div>
  <div class="ps-3">
    <nav aria-label="breadcrumb">
      <ol class="breadcrumb mb-0 p-0">
        <li class="breadcrumb-item"><a href="/admin"><i class="bx bx-home-alt"></i></a>
        </li>
        <li class="breadcrumb-item active" aria-current="page"><?= $admin_page_title; ?></li>
      </ol>
    </nav>
  </div>
</div>