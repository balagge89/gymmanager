<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('admin/header'); ?>

<!--wrapper-->

<div class="page-content d-flex  justify-content-center my-5 my-lg-0">



    <div class="container-fluid">

        <div class="row row-cols-10 row-cols-lg-10 row-cols-xl-10">
            <div class="col mx-auto">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <h3 class="mb-4 text-center"><?= $admin_page_title ?></h3>
                            </div>
                            <div class="ms-auto">
                                <a href="javascript:;" class="btn btn-primary radius-30 mt-2 mt-lg-0" data-bs-toggle="modal" data-bs-target="#addNewTicketModal">
                                    <i class="bx bxs-plus-square"></i><?= $this->lang->line('admin_tickets_add_new'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-middle mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th><?= $this->lang->line('admin_tickets_id'); ?></th>
                                        <th><?= $this->lang->line('admin_tickets_name'); ?></th>
                                        <th><?= $this->lang->line('admin_tickets_occasion'); ?></th>
                                        <th><?= $this->lang->line('admin_tickets_valid'); ?></th>
                                        <th><?= $this->lang->line('admin_tickets_price'); ?></th>
                                        <th><?= $this->lang->line('admin_tickets_modify'); ?></th>
                                        <th><?= $this->lang->line('admin_tickets_delete'); ?></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($ticket_list_data as $ticket) { ?>
                                        <tr>
                                            <td><?= $ticket->id ?></a></td>
                                            <td><?= $ticket->ticket_name ?></td>
                                            <td><?= $ticket->occasion ?></td>
                                            <td><?= $ticket->day ?></td>
                                            <td><?= $ticket->price ?></td>
                                            <td><a href="#" class="" data-bs-toggle="modal" data-bs-target="#ticketModal<?= $ticket->id ?>"><i class="bx bxs-edit"></i></a></td>
                                            <td><a href="/admin/tickets/delete/<?= $ticket->id ?>" class="" onclick="return confirm('<?= $this->lang->line('admin_tickets_delete_confirm'); ?>');"><i class="bx bxs-trash"></i></a></td>
                                        </tr>
                                        <div class="modal fade" id="ticketModal<?= $ticket->id ?>" tabindex="-1" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"><i class="bx bx-id-card"></i> <?= $ticket->ticket_name ?></h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <?= validation_errors(); ?>

                                                    <?= form_open('admin/tickets/update'); ?>
                                                    <div class="modal-body">

                                                        <input class="form-control mb-3" type="number" name="ticket_id" value="<?= $ticket->id ?>" aria-label="default input example" required hidden>

                                                        <?= $this->lang->line('admin_tickets_name'); ?>
                                                        <input class="form-control mb-3" type="text" name="ticket_name" placeholder="<?= $this->lang->line('admin_tickets_name'); ?>" value="<?= $ticket->ticket_name ?>" aria-label="default input example" required>

                                                        <?= $this->lang->line('admin_tickets_occasion'); ?>
                                                        <input class="form-control mb-3" type="number" name="ticket_occasion" placeholder="<?= $this->lang->line('admin_tickets_occasion'); ?>" value="<?= $ticket->occasion ?>" aria-label="default input example" required>

                                                        <?= $this->lang->line('admin_tickets_price'); ?>
                                                        <input class="form-control mb-3" type="number" name="ticket_price" placeholder="<?= $this->lang->line('admin_tickets_price'); ?>" value="<?= $ticket->price ?>" aria-label="default input example" required>

                                                        <?= $this->lang->line('admin_tickets_valid'); ?>
                                                        <input class="form-control mb-3" type="number" name="ticket_valid" placeholder="<?= $this->lang->line('admin_tickets_valid'); ?>" value="<?= $ticket->day ?>" aria-label="default input example" required>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary"><?= $this->lang->line('admin_tickets_modify'); ?></button>
                                                    </div>
                                                    <?= form_close(); ?>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>

                                </tbody>
                            </table>
                            <div class="modal fade" id="addNewTicketModal" tabindex="-1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"><i class="bx bx-id-card"></i> <?= $this->lang->line('admin_tickets_add_new'); ?></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <?= validation_errors(); ?>

                                        <?= form_open('admin/tickets/add_new'); ?>
                                        <div class="modal-body">

                                            <?= $this->lang->line('admin_tickets_name'); ?>
                                            <input class="form-control mb-3" type="text" name="ticket_name" placeholder="<?= $this->lang->line('admin_tickets_name'); ?>" aria-label="default input example" required>

                                            <?= $this->lang->line('admin_tickets_occasion'); ?>
                                            <input class="form-control mb-3" type="number" name="ticket_occasion" placeholder="<?= $this->lang->line('admin_tickets_occasion'); ?>" aria-label="default input example" required>

                                            <?= $this->lang->line('admin_tickets_price'); ?>
                                            <input class="form-control mb-3" type="number" name="ticket_price" placeholder="<?= $this->lang->line('admin_tickets_price'); ?>" aria-label="default input example" required>


                                            <?= $this->lang->line('admin_tickets_valid'); ?>
                                            <input class="form-control mb-3" type="number" name="ticket_valid" placeholder="<?= $this->lang->line('admin_tickets_valid'); ?>" aria-label="default input example" required>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary"><?= $this->lang->line('admin_tickets_add'); ?></button>
                                        </div>
                                        <?= form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>
</div>

<?php $this->load->view('admin/footer'); ?>
</body>

</html>