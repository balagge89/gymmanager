<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('admin/header'); ?>

    <!--wrapper-->

        <div class="page-content d-flex  justify-content-center my-5 my-lg-0">



            <div class="container-fluid">
                
                <div class="row row-cols-10 row-cols-lg-10 row-cols-xl-10">
                    <div class="col mx-auto">
                        <div class="card radius-10">
                            <div class="card-body">
                                <div class="d-flex align-items-center">
                                    <div>
                                        <h3 class="mb-4 text-center"><?= $admin_page_title ?>: <?= $daily_checkin_data['daily_checkins_sum']; ?></h3>
                                    </div>
                                    <div class="dropdown ms-auto">
                                        <a class="dropdown-toggle dropdown-toggle-nocaret" href="#" data-bs-toggle="dropdown"><i class="bx bx-dots-horizontal-rounded font-22 text-option"></i>
                                        </a>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="javascript:;">Action</a>
                                            </li>
                                            <li><a class="dropdown-item" href="javascript:;">Another action</a>
                                            </li>
                                            <li>
                                                <hr class="dropdown-divider">
                                            </li>
                                            <li><a class="dropdown-item" href="javascript:;">Something else here</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table align-middle mb-0">
                                        <thead class="table-light">
                                            <tr>
                                                <th>Név</th>
                                                <th>Bérlet Száma</th>
                                                <th>Bérlet Típusa</th>
                                                <th>Bérlet Érvényesség</th>
                                                <th>Hátralévő Alkalmak</th>
                                                <th>Időpont</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach($daily_checkin_data['daily_checkins_list'] as $checkin_data) { ?>
                                            <tr>
                                                <td><img src="<?= $checkin_data->img_url ?>" class="product-img-2" alt="product img"><a class="user_name_link" href="/admin/profile/<?= $checkin_data->id ?>"><?= $checkin_data->name ?></a></td>
                                                <td><?= $checkin_data->ticket_number ?></td>
                                                <td><?= $checkin_data->ticket_type ?></td>
                                                <td><?= $checkin_data->ticket_valid ?></td>
                                                <td><?= $checkin_data->ticket_occasion ?></td>
                                                <td><?= $checkin_data->date ?></td>
                                            </tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
            </div>
        </div>
    </div>
</body>

</html>

<?php $this->load->view('admin/footer'); ?>