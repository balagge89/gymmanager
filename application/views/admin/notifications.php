<?php if (isset($_SESSION['ticket_edit_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_tickets_success_update'); ?>";
		success_noti(message);
		</script>
<?php } 

if (isset($_SESSION['ticket_add_new_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_tickets_success_add'); ?>";
		success_noti(message);
		</script>
<?php }

if (isset($_SESSION['ticket_delete_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_tickets_success_delete'); ?>";
		success_noti(message);
		</script>
<?php } ?>

<?php if (isset($_SESSION['product_edit_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_products_success_update'); ?>";
		success_noti(message);
		</script>
<?php } 

if (isset($_SESSION['product_add_new_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_products_success_add'); ?>";
		success_noti(message);
		</script>
<?php }

if (isset($_SESSION['products_delete_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_products_success_delete'); ?>";
		success_noti(message);
		</script>
<?php } ?>


<?php if (isset($_SESSION['admin_edit_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_admins_success_update'); ?>";
		success_noti(message);
		</script>
<?php } 

if (isset($_SESSION['admin_add_new_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_admins_success_add'); ?>";
		success_noti(message);
		</script>
<?php }

if (isset($_SESSION['admin_delete_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_admins_success_delete'); ?>";
		success_noti(message);
		</script>
<?php } ?>

<?php if (isset($_SESSION['coach_edit_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_coaches_success_update'); ?>";
		success_noti(message);
		</script>
<?php } 

if (isset($_SESSION['coach_add_new_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_coaches_success_add'); ?>";
		success_noti(message);
		</script>
<?php }

if (isset($_SESSION['coach_delete_success'])) { ?>
	<script>
		var message = "<?= $this->lang->line('admin_coaches_success_delete'); ?>";
		success_noti(message);
		</script>
<?php } ?>