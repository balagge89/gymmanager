<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('admin/header'); ?>
<div class="page-content d-flex  justify-content-center my-5 my-lg-0">



    <div class="container-fluid">

        <div class="row row-cols-10 row-cols-lg-10 row-cols-xl-10">
            <div class="col mx-auto">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <h3 class="mb-4 text-center"><?= $admin_page_title ?></h3>
                            </div>
                            <div class="ms-auto">
                                <a href="javascript:;" class="btn btn-primary radius-30 mt-2 mt-lg-0" data-bs-toggle="modal" data-bs-target="#addNewTicketModal">
                                    <i class="bx bxs-plus-square"></i><?= $this->lang->line('admin_tickets_add_new'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <div class="card">
                                <div class="card-body">
                                    <label for="exampleDataList" class="form-label"><?= $this->lang->line('admin_users_search_for'); ?></label>
                                    <input class="form-control" list="datalistOptions" name="search_text" id="search_text" placeholder="<?= $this->lang->line('admin_users_search'); ?>">
                                </div>
                            </div>
                            <div id="result"></div>
                            <script>
                                $(document).ready(function() {
                                    load_data();

                                    function load_data(query) {
                                        $.ajax({
                                            url: "<?php echo base_url(); ?>admin/users/fetch_user",
                                            method: "POST",
                                            data: {
                                                query: query
                                            },
                                            success: function(data) {
                                                $('#result').html(data);
                                            }
                                        })
                                    }
                                    $('#search_text').keyup(function() {
                                        var search = $(this).val();
                                        if (search != '') {
                                            load_data(search);
                                        } else {
                                            load_data();
                                        }
                                    });
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>
</div>

<?php $this->load->view('admin/footer'); ?>
</body>

</html>