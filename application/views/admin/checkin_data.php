<?php
if (empty($checkin_data)) {
    $ticket_bg = "warning";
    $ticket_state = -1;
} elseif (($checkin_data[0]->ticket_valid < date("Y-m-d")) || ($checkin_data[0]->ticket_occasion < 1)) {
    $ticket_bg = "danger";
    $ticket_state = 0;
} elseif ($checkin_data[0]->ticket_valid >= date("Y-m-d")) {
    $ticket_bg = "success";
    $ticket_state = 1;
}

?>
<div class="wrapper p-3">
    <div class="row row-cols-12 row-cols-lg-12 row-cols-xl-12">
        <div class="col">
            <div class="card radius-15 bg-<?= $ticket_bg; ?>">
                <div class="card-body">
                    <div class="row">
                        <?php if ($ticket_state > -1) { ?>
                            <div class="col col-12 col-lg-6 col-xl-6 text-right">
                                <div class="p-4 radius-15">
                                    <img src="<?= $checkin_data[0]->img_url ?>" class="mw-100 checkin-profile-picture rounded-circle shadow p-1 bg-white pull-right" alt="">

                                </div>
                            </div>

                            <div class="col col-12 col-lg-6 col-xl-6 text-left">
                                <div class="p-4 radius-15">
                                    <h1 class="mb-4 mt-5 text-white"><?= $checkin_data[0]->name ?></h1>
                                    <h5 class="mb-3 text-white"><?= $this->lang->line('admin_checkin_ticket_type'); ?>: <?= $checkin_data[0]->ticket_type ?></h5>
                                    <h5 class="mb-3 text-white"><?= $this->lang->line('admin_checkin_ticket_number'); ?>: <?= $checkin_data[0]->ticket_number ?></h5>
                                    <h5 class="mb-3 text-white"><?= $this->lang->line('admin_checkin_ticket_valid'); ?>: <?= $checkin_data[0]->ticket_valid ?></h5>
                                    <h5 class="mb-3 text-white"><?= $this->lang->line('admin_checkin_ticket_occasion'); ?>: <?= $checkin_data[0]->ticket_occasion - 1 ?></h5>
                                    <h5 class="mb-3 text-white"><?= $this->lang->line('admin_checkin_ticket_payment'); ?>: <?= $checkin_data[0]->ticket_payment ?></h5>

                                    <?php if ($ticket_state == 0) { ?>
                                        <div class="d-grid w-50 mt-5"> <a href="/admin/profile/<?= $checkin_data[0]->id ?>" target="_blank" class="btn btn-danger border-white radius-15"><?= $this->lang->line('admin_checkin_ticket_renew'); ?></a>
                                        <?php } ?>
                                        </div>
                                </div>
                            </div>
                        <?php }
                            elseif ($ticket_state == -1) { ?>
                            <div class="col col-12 col-lg-12 col-xl-12 text-center">
                                <div class="p-5 radius-15">
                                    <h1 class="text-white"><?= $this->lang->line('admin_checkin_ticket_wrong_number'); ?></h1>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>