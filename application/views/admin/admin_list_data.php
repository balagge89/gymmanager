<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('admin/header'); ?>

<!--wrapper-->

<div class="page-content d-flex  justify-content-center my-5 my-lg-0">



    <div class="container-fluid">

        <div class="row row-cols-10 row-cols-lg-10 row-cols-xl-10">
            <div class="col mx-auto">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <h3 class="mb-4 text-center"><?= $admin_page_title ?></h3>
                            </div>
                            <div class="ms-auto">
                                <a href="javascript:;" class="btn btn-primary radius-30 mt-2 mt-lg-0" data-bs-toggle="modal" data-bs-target="#addNewadminModal">
                                    <i class="bx bxs-plus-square"></i><?= $this->lang->line('admin_products_add_new'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-middle mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th><?= $this->lang->line('admin_admins_id'); ?></th>
                                        <th><?= $this->lang->line('admin_admins_name'); ?></th>
                                        <th><?= $this->lang->line('admin_admins_user'); ?></th>
                                        <th><?= $this->lang->line('admin_admins_modify'); ?></th>
                                        <th><?= $this->lang->line('admin_admins_delete'); ?></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($admin_list_data as $admin) { ?>
                                        <tr>
                                            <td><?= $admin->id ?></a></td>
                                            <td><?= $admin->name ?></td>
                                            <td><?= $admin->user ?></td>
                                            <td><a href="#" class="" data-bs-toggle="modal" data-bs-target="#adminModal<?= $admin->id ?>"><i class="bx bxs-edit"></i></a></td>
                                            <td><a href="/admin/admins/delete/<?= $admin->id ?>" class="" onclick="return confirm('<?= $this->lang->line('admin_admins_delete_confirm'); ?>');"><i class="bx bxs-trash"></i></a></td>
                                        </tr>
                                        <div class="modal fade" id="adminModal<?= $admin->id ?>" tabindex="-1" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"><i class="bx bx-user-check"></i> <?= $admin->name ?></h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <?= validation_errors(); ?>

                                                    <?= form_open_multipart('admin/admins/update'); ?>
                                                    <div class="modal-body">

                                                        <input class="form-control mb-3" type="text" name="admin_id" value="<?= $admin->id ?>" hidden>

                                                        <?= $this->lang->line('admin_admins_name'); ?>
                                                        <input class="form-control mb-3" type="text" name="admin_name" value="<?= $admin->name ?>" placeholder="<?= $this->lang->line('admin_admins_name'); ?>" aria-label="default input example">

                                                        <?= $this->lang->line('admin_admins_user'); ?>
                                                        <input class="form-control mb-3" type="text" name="admin_user" value="<?= $admin->user ?>" placeholder="<?= $this->lang->line('admin_admins_user'); ?>" aria-label="default input example">

                                                        <?= $this->lang->line('admin_admins_password'); ?>
                                                        <input class="form-control mb-3" type="password" name="admin_password" id="password" placeholder="<?= $this->lang->line('admin_admins_password'); ?>" aria-label="default input example">

                                                        <?= $this->lang->line('admin_admins_password_confirm'); ?>
                                                        <input onclick="password_match_check();" class="form-control mb-3" type="password" name="admin_password_confirm" id="confirm_password" placeholder="<?= $this->lang->line('admin_admins_password_confirm'); ?>" aria-label="default input example">
                                                        <div id="password_match_message"></div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary"><?= $this->lang->line('admin_admins_modify'); ?></button>
                                                    </div>
                                                    <?= form_close(); ?>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>

                                </tbody>
                            </table>
                            <div class="modal fade" id="addNewadminModal" tabindex="-1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"><i class="bx bx-user-plus"></i> <?= $this->lang->line('admin_products_add_new'); ?></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <?= validation_errors(); ?>

                                        <?= form_open_multipart('admin/admins/add_new'); ?>
                                        <div class="modal-body">

                                            <?= $this->lang->line('admin_admins_name'); ?>
                                            <input class="form-control mb-3" type="text" name="admin_name" placeholder="<?= $this->lang->line('admin_admins_name'); ?>" aria-label="default input example">

                                            <?= $this->lang->line('admin_admins_user'); ?>
                                            <input class="form-control mb-3" type="text" name="admin_user" placeholder="<?= $this->lang->line('admin_admins_user'); ?>" aria-label="default input example">

                                            <?= $this->lang->line('admin_admins_password'); ?>
                                            <input class="form-control mb-3" type="password" name="admin_password" id="password" placeholder="<?= $this->lang->line('admin_admins_password'); ?>" aria-label="default input example">

                                            <?= $this->lang->line('admin_admins_password_confirm'); ?>
                                            <input class="form-control mb-3" onclick="password_match_check();" type="password" name="admin_password_confirm" id="confirm_password" placeholder="<?= $this->lang->line('admin_admins_password_confirm'); ?>" aria-label="default input example">
                                            <div id="password_match_message"></div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary"><?= $this->lang->line('admin_admins_add'); ?></button>
                                        </div>
                                        <?= form_close(); ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>
</div>
<script type="text/javascript">
var match = "<?= $this->lang->line('admin_admins_password_match'); ?>";
var not_match = "<?= $this->lang->line('admin_admins_password_not_match'); ?>";
</script>
<?php $this->load->view('admin/footer'); ?>
</body>

</html>