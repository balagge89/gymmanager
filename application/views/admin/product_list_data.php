<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('admin/header'); ?>

<!--wrapper-->

<div class="page-content d-flex  justify-content-center my-5 my-lg-0">



    <div class="container-fluid">

        <div class="row row-cols-10 row-cols-lg-10 row-cols-xl-10">
            <div class="col mx-auto">
                <div class="card radius-10">
                    <div class="card-body">
                        <div class="d-flex align-items-center">
                            <div>
                                <h3 class="mb-4 text-center"><?= $admin_page_title ?></h3>
                            </div>
                            <div class="ms-auto">
                                <a href="javascript:;" class="btn btn-primary radius-30 mt-2 mt-lg-0" data-bs-toggle="modal" data-bs-target="#addNewproductModal">
                                    <i class="bx bxs-plus-square"></i><?= $this->lang->line('admin_products_add_new'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table align-middle mb-0">
                                <thead class="table-light">
                                    <tr>
                                        <th><?= $this->lang->line('admin_products_id'); ?></th>
                                        <th><?= $this->lang->line('admin_products_img'); ?></th>
                                        <th><?= $this->lang->line('admin_products_name'); ?></th>
                                        <th><?= $this->lang->line('admin_products_desc'); ?></th>
                                        <th><?= $this->lang->line('admin_products_stock'); ?></th>
                                        <th><?= $this->lang->line('admin_products_price'); ?></th>
                                        <th><?= $this->lang->line('admin_products_barcode'); ?></th>
                                        <th><?= $this->lang->line('admin_products_modify'); ?></th>
                                        <th><?= $this->lang->line('admin_products_delete'); ?></th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($product_list_data as $product) { ?>
                                        <tr>
                                            <td><?= $product->id ?></a></td>
                                            <td><span class="product_img_thumb" style="background-image: url('<?= base_url('assets/admin/img/products/').$product->product_img ?>');"></span></td>
                                            <td><?= $product->product_name ?></td>
                                            <td><?= $product->product_desc ?></td>
                                            <td><?= $product->product_stock ?></td>
                                            <td><?= $product->product_price ?></td>
                                            <td><?= $product->product_barcode ?></td>
                                            <td><a href="#" class="" data-bs-toggle="modal" data-bs-target="#productModal<?= $product->id ?>"><i class="bx bxs-edit"></i></a></td>
                                            <td><a href="/admin/products/delete/<?= $product->id ?>" class="" onclick="return confirm('<?= $this->lang->line('admin_products_delete_confirm'); ?>');"><i class="bx bxs-trash"></i></a></td>
                                        </tr>
                                        <div class="modal fade" id="productModal<?= $product->id ?>" tabindex="-1" style="display: none;" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"><i class="bx bx-package"></i> <?= $product->product_name ?></h5>
                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                    </div>
                                                    <?= validation_errors(); ?>

                                                    <?= form_open_multipart('admin/products/update'); ?>
                                                    <div class="modal-body">

                                                    <input class="form-control mb-3" type="text" name="product_id" value="<?= $product->id ?>" hidden>

                                                    <?= $this->lang->line('admin_products_name'); ?>
                                                    <input class="form-control mb-3" type="text" name="product_name" value="<?= $product->product_name ?>" placeholder="<?= $this->lang->line('admin_products_name'); ?>" aria-label="default input example" >

                                                    <?= $this->lang->line('admin_products_img'); ?>
                                                    <input class="form-control mb-3" type="file" name="product_img" placeholder="<?= $this->lang->line('admin_products_img'); ?>" aria-label="default input example" >

                                                    <?= $this->lang->line('admin_products_desc'); ?>
                                                    <input class="form-control mb-3" type="text" name="product_desc" value="<?= $product->product_desc ?>" placeholder="<?= $this->lang->line('admin_products_desc'); ?>" aria-label="default input example" >

                                                    <?= $this->lang->line('admin_products_stock'); ?>
                                                    <input class="form-control mb-3" type="number" name="product_stock" value="<?= $product->product_stock ?>" placeholder="<?= $this->lang->line('admin_products_stock'); ?>" aria-label="default input example" >


                                                    <?= $this->lang->line('admin_products_price'); ?>
                                                    <input class="form-control mb-3" type="number" name="product_price" value="<?= $product->product_price ?>" placeholder="<?= $this->lang->line('admin_products_price'); ?>" aria-label="default input example" >

                                                    <?= $this->lang->line('admin_products_barcode'); ?>
                                                    <input class="form-control mb-3" type="number" name="product_barcode" value="<?= $product->product_barcode ?>" placeholder="<?= $this->lang->line('admin_products_barcode'); ?>" aria-label="default input example" >

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary"><?= $this->lang->line('admin_products_modify'); ?></button>
                                                    </div>
                                                    <?= form_close(); ?>
                                                </div>
                                            </div>
                                        </div>

                                    <?php } ?>

                                </tbody>
                            </table>
                            <div class="modal fade" id="addNewproductModal" tabindex="-1" style="display: none;" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title"><i class="bx bx-package"></i> <?= $this->lang->line('admin_products_add_new'); ?></h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <?= validation_errors(); ?>

                                        <?= form_open_multipart('admin/products/add_new'); ?>
                                        <div class="modal-body">
                                            <?= $this->lang->line('admin_products_name'); ?>
                                            <input class="form-control mb-3" type="text" name="product_name" placeholder="<?= $this->lang->line('admin_products_name'); ?>" aria-label="default input example" required>

                                            <?= $this->lang->line('admin_products_img'); ?>
                                            <input class="form-control mb-3" type="file" name="product_img" placeholder="<?= $this->lang->line('admin_products_img'); ?>" aria-label="default input example" required>

                                            <?= $this->lang->line('admin_products_desc'); ?>
                                            <input class="form-control mb-3" type="text" name="product_desc" placeholder="<?= $this->lang->line('admin_products_desc'); ?>" aria-label="default input example" required>

                                            <?= $this->lang->line('admin_products_stock'); ?>
                                            <input class="form-control mb-3" type="number" name="product_stock" placeholder="<?= $this->lang->line('admin_products_stock'); ?>" aria-label="default input example" required>


                                            <?= $this->lang->line('admin_products_price'); ?>
                                            <input class="form-control mb-3" type="number" name="product_price" placeholder="<?= $this->lang->line('admin_products_price'); ?>" aria-label="default input example" required>

                                            <?= $this->lang->line('admin_products_barcode'); ?>
                                            <input class="form-control mb-3" type="number" name="product_barcode" placeholder="<?= $this->lang->line('admin_products_barcode'); ?>" aria-label="default input example" required>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-primary"><?= $this->lang->line('admin_products_add'); ?></button>
                                        </div>
                                        <?= form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end row-->
    </div>
</div>
</div>

<?php $this->load->view('admin/footer'); ?>
</body>

</html>