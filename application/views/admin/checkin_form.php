<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('admin/header'); ?>
<script>
	document.onload = setInterval(function() {
		document.getElementById('ticket_number').focus();
	}, 1000);
</script>

<body>


    <!--wrapper-->
    <div class="wrapper p-2">
        <div class="d-flex align-items-center justify-content-center my-5 my-lg-0">
            <div class="container-fluid">
                <div class="row row-cols-12 row-cols-lg-12 row-cols-xl-12">
                    <div class="col mx-auto">
                        <div class="card rounded-4">
                            <div class="card-body">
                                <div class="border p-4 rounded-4">
                                    <div class="text-center">
                                        <img src="assets/images/logo-icon.png" width="70" alt="" />
                                        <h5 class="mt-0 mb-4">Gym Manager - <?= $this->lang->line('admin_checkin_button'); ?></h5>
                                    </div>

                                    <div class="form-body">
                                        <form class="row g-3" method="post" action="<?= $_SERVER['REQUEST_URI']; ?>">
                                            <div class="col-12">
                                                <input type="number" autocomplete="off" name="ticket_number" id="ticket_number" class="form-control rounded-5" placeholder="<?= $this->lang->line('admin_checkin_ticket_number'); ?>" required autofocus>
                                            </div>
                                            <div class="col-12">
                                                <div class="d-grid">
                                                    <button type="submit" class="btn btn-gradient-info rounded-5"><?= $this->lang->line('admin_checkin_button'); ?></button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
            </div>
        </div>
    </div>
