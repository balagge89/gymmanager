<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('admin/header'); ?>

<body>


    <!--wrapper-->

        
            <div class="container-fluid">
                <div class="row row-cols-1 row-cols-lg-2 row-cols-xl-3">
                    <div class="col mx-auto">
                        <div class="card rounded-4">
                            <div class="card-body">
                                <div class="border p-4 rounded-4">
                                    <div class="text-center">
                                        <img src="assets/images/logo-icon.png" width="70" alt="" />
                                        <h2 class="mt-3 mb-20">Gym Manager Admin</h2>
                                        <p class="mb-4"><?= $this->lang->line('admin_login_subtitle'); ?></p>
                                    </div>

                                    <div class="form-body">
                                        <form class="row g-3" method="post" action="<?php echo site_url('admin/login/process'); ?>">
                                            <div class="col-12">
                                                <input type="text" name="user" class="form-control rounded-5" id="inputUserName" placeholder="<?= $this->lang->line('admin_login_username'); ?>" required>
                                            </div>
                                            <div class="col-12">

                                                <input type="password" name="pass" class="form-control rounded-5" id="inputChoosePassword" placeholder="<?= $this->lang->line('admin_login_password'); ?>" required>
                                            </div>

                                            <div class="col-12">
                                                <div class="d-grid">
                                                    <button type="submit" class="btn btn-gradient-info rounded-5"><i class="bx bxs-lock-open"></i><?= $this->lang->line('admin_login_button'); ?></button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end row-->
            </div>
        

</body>

</html>

<?php $this->load->view('admin/footer'); ?>