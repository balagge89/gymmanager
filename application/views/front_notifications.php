<!-- Max Table Modify Notification -->
<?php if (isset($_SESSION['max_edit_success'])) { ?>
  <script>
    md.showNotification('top', 'center', 'success', '<?= $this->lang->line('max_table_success_modify'); ?>');
  </script>
<?php } ?>

<!-- Date Registration Notification -->
<?php if (isset($_SESSION['event_reg_result'])) {

  //If registration is successful
  if ($_SESSION['event_reg_result']['result'] == 1) {
    $notification_state = "success";
  }
  //If registration is unsuccessful
  elseif ($_SESSION['event_reg_result']['result'] == 0) {
    $notification_state = "danger";
  } ?>
  <script>
    md.showNotification('top', 'center', '<?= $notification_state; ?>', '<?= $_SESSION['event_reg_result']['message']; ?>');
  </script>
<?php }

// Date Unregistration Notification
if (isset($_SESSION['event_unreg_result'])) {
  //If registration is successful
  if ($_SESSION['event_unreg_result']['result'] == 1) {
    $notification_state = "success";
  }
  //If registration is unsuccessful
  elseif ($_SESSION['event_unreg_result']['result'] == 0) {
    $notification_state = "danger";
  } ?>
  <script>
    md.showNotification('top', 'center', '<?= $notification_state; ?>', '<?= $_SESSION['event_unreg_result']['message']; ?>');
  </script>
<?php }  ?>