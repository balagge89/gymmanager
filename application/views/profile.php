<div class="tab-pane" id="profile">

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-primary menu-header">
                            <h4 class="card-title"><?= $this->lang->line('menu_profile'); ?></h4>
                            <p class="card-category"><?= $this->lang->line('profile_data'); ?></p>
                        </div>
                        <div class="card-body">
                            <img class="img" src="<?= $user_data[0]->img_url ?>" />
                            <h2 class="card-title"><?= $user_data[0]->name; ?></h2>
                            <img class="img" src="<?= base_url('assets/front/img/qr-code/' . $user_data[0]->ticket_number . '.png') ?>" />
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><?= $this->lang->line('profile_ticket_number'); ?></td>
                                        <td><?= $user_data[0]->ticket_number; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= $this->lang->line('profile_ticket_occasion'); ?></td>
                                        <td><?= $user_data[0]->ticket_occasion; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= $this->lang->line('profile_ticket_valid'); ?></td>
                                        <td><?= $user_data[0]->ticket_valid; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= $this->lang->line('profile_ticket_type'); ?></td>
                                        <td><?= $user_data[0]->ticket_type; ?></td>
                                    </tr>
                                    <tr>
                                        <td><?= $this->lang->line('profile_ticket_payment'); ?></td>
                                        <td><?= $user_data[0]->ticket_payment; ?></td>
                                    </tr>
                                </tbody>
                            </table>



                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-primary menu-header">
                            <h4 class="card-title"><?= $this->lang->line('profile_my_max'); ?></h4>
                            <p class="card-category"><?= $this->lang->line('profile_upload_max'); ?></p>
                        </div>
                        <div class="card-body">
                            <table class="table">
                                <tbody>

                                    <tr>
                                        <td>
                                            Clean & Jerk<br>
                                            <?= $user_maxes[0]->clean_jerk ?? '0'; ?>
                                        </td>
                                        <td>
                                            Snatch<br>
                                            <?= $user_maxes[0]->snatch ?? '0'; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            OHS<br>
                                            <?= $user_maxes[0]->ohs ?? '0'; ?>
                                        </td>
                                        <td>
                                            Deadlift<br>
                                            <?= $user_maxes[0]->deadlift ?? '0'; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Back Squat<br>
                                            <?= $user_maxes[0]->back_squat ?? '0'; ?>
                                        </td>
                                        <td>
                                            Front Squat<br>
                                            <?= $user_maxes[0]->front_squat ?? '0'; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Shoulder Press<br>
                                            <?= $user_maxes[0]->shoulder_press ?? '0'; ?>
                                        </td>
                                        <td>
                                            Squat Clean<br>
                                            <?= $user_maxes[0]->squat_clean ?? '0'; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Thruster<br>
                                            <?= $user_maxes[0]->thruster ?? '0'; ?>
                                        </td>
                                        <td>
                                            Push Press<br>
                                            <?= $user_maxes[0]->push_press ?? '0'; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Bear Complex<br>
                                            <?= $user_maxes[0]->bear_complex ?? '0'; ?>
                                        </td>
                                        <td>
                                            100 Burpee<br>
                                            <?= $user_maxes[0]->burpee ?? '0'; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Double Unders<br>
                                            <?= $user_maxes[0]->double_under ?? '0'; ?>
                                        </td>
                                        <td>
                                            Run 5K<br>
                                            <?= $user_maxes[0]->run_5k ?? '0'; ?>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            Handstand Walk<br>
                                            <?= $user_maxes[0]->double_under ?? '0'; ?>
                                        </td>
                                        <td>
                                            Weight<br>
                                            <?= $user_maxes[0]->weight ?? '0'; ?>
                                        </td>
                                    </tr>

                                </tbody>
                            </table>
                            <span class="text-center"><a href="#" data-toggle="modal" data-target="#MaxEditingForm"><?= $this->lang->line('profile_edit_max'); ?></a></span>
                            <!-- Modal -->
                            <div class="modal fade" id="MaxEditingForm" tabindex="-1" role="dialog" aria-labelledby="MaxEditingForm" aria-hidden="true">
                                <div class="modal-dialog" role="document">

                                    <div class="card card-chart">

                                        <div class="card-header card-header-primary menu-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <h4 class="card-title"><?= $this->lang->line('profile_my_max'); ?></h4>
                                        </div>
                                        <div class="card-body">
                                            <?= validation_errors(); ?>

                                            <?= form_open('max_form_submit'); ?>

                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Clean & Jerk</label>
                                                        <input type="text" name="clean_jerk" class="form-control" value="<?= $user_maxes[0]->clean_jerk ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Snatch</label>
                                                        <input type="text" name="snatch" class="form-control" value="<?= $user_maxes[0]->snatch ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">OHS</label>
                                                        <input type="text" name="ohs" class="form-control" value="<?= $user_maxes[0]->ohs ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Deadlift</label>
                                                        <input type="text" name="deadlift" class="form-control" value="<?= $user_maxes[0]->deadlift ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Back Squat</label>
                                                        <input type="text" name="back_squat" class="form-control" value="<?= $user_maxes[0]->back_squat ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Front Squat</label>
                                                        <input type="text" name="front_squat" class="form-control" value="<?= $user_maxes[0]->front_squat ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Shoulder Press</label>
                                                        <input type="text" name="shoulder_press" class="form-control" value="<?= $user_maxes[0]->shoulder_press ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Squat Clean</label>
                                                        <input type="text" name="squat_clean" class="form-control" value="<?= $user_maxes[0]->squat_clean ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Thruster</label>
                                                        <input type="text" name="thruster" class="form-control" value="<?= $user_maxes[0]->thruster ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Push Press</label>
                                                        <input type="text" name="push_press" class="form-control" value="<?= $user_maxes[0]->push_press ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Bear Complex</label>
                                                        <input type="text" name="bear_complex" class="form-control" value="<?= $user_maxes[0]->bear_complex ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">100 Burpee</label>
                                                        <input type="text" name="burpee" class="form-control" value="<?= $user_maxes[0]->burpee ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Double Unders</label>
                                                        <input type="text" name="double_under" class="form-control" value="<?= $user_maxes[0]->double_under ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Run 5K</label>
                                                        <input type="text" name="run_5k" class="form-control" value="<?= $user_maxes[0]->run_5k ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Handstand Walk</label>
                                                        <input type="text" name="handstand_walk" class="form-control" value="<?= $user_maxes[0]->handstand_walk ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-group bmd-form-group">
                                                        <label class="bmd-label-floating">Body Weight</label>
                                                        <input type="text" name="weight" class="form-control" value="<?= $user_maxes[0]->weight ?? '0'; ?>">
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-group bmd-form-group">
                                                        <button type="submit" class="btn btn-primary application"><?= $this->lang->line('profile_save_max'); ?><div class="ripple-container"></div></button>
                                                    </div>
                                                </div>

                                            </div>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="card">
                        <div class="card-header card-header-primary menu-header">
                            <h4 class="card-title"><?= $this->lang->line('profile_last_events'); ?></h4>
                            <p class="card-category"><?= $count_user_regs; ?></p>
                        </div>
                        <div class="card-body">

                            <table class="table">
                                <thead>
                                    <tr>
                                        <th><?= $this->lang->line('profile_day'); ?></th>
                                        <th><?= $this->lang->line('profile_type'); ?></th>
                                        <th><?= $this->lang->line('profile_date'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($user_regs as $user_regs_data) { ?>
                                        <tr>
                                            <td><?=  $user_regs_data->day; ?></td>
                                            <td><?=  $user_regs_data->category; ?></td>
                                            <td><?=  $user_regs_data->date; ?></td>
                                        </tr>
                                    <? } ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</div>