
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Gym Manager</title>

    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/front/img/favicon.svg');?>">
    <link rel="icon" type="image/png" href="<?php echo base_url('assets/front/img/favicon.svg');?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <!-- CSS Files -->
    <link href="<?php echo base_url('assets/front/css/material-dashboard.css?v=2.1.2');?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/front/css/custom.css');?>" rel="stylesheet" /> 
</head>
<body>