                <!-- Navbar -->
                <div class="menu-header card-header card-header-tabs card-header-primary">

                    <div class="nav-tabs-navigation desktop">
                        <div class="nav-tabs-wrapper">
                            <ul class="nav nav-tabs left" data-tabs="tabs">
                                <li class="nav-item">
                                    <a class="nav-link active show" href="#dates" data-toggle="tab">
                                        <i class="material-icons">free_cancellation</i> <?php echo $this->lang->line('menu_dates'); ?>
                                        <div class="ripple-container"></div>
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#profile" data-toggle="tab">
                                        <i class="material-icons">account_circle</i> <?php echo $this->lang->line('menu_profile'); ?>
                                        <div class="ripple-container"></div>
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#max-table" data-toggle="tab">
                                        <i class="material-icons">table_chart</i> <?php echo $this->lang->line('menu_max_table'); ?>
                                        <div class="ripple-container"></div>
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="" data-toggle="modal" data-target="#TopMonthModal">
                                        <i class="material-icons">format_list_bulleted</i> <?php echo $this->lang->line('menu_top_month'); ?>
                                        <div class="ripple-container"></div>
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="nav-tabs-navigation desktop">
                        <div class="nav-tabs-wrapper">
                            <ul class="nav nav-tabs right" data-tabs="tabs">
                                <li class="nav-item">
                                    <a class="nav-link" href="logout">
                                        <i class="material-icons">logout</i> <?php echo $this->lang->line('menu_logout'); ?>
                                        <div class="ripple-container"></div>
                                        <div class="ripple-container"></div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="nav-tabs-navigation mobile">
                        <div class="nav-tabs-wrapper ">
                            <ul class="nav navbar-nav nav-mobile-menu left">
                                <li class="nav-item dropdown">
                                    <a class="nav-link" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="material-icons">menu</i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right showing hiding" aria-labelledby="navbarDropdownMenuLink" x-placement="bottom-end" style="position: absolute; top: 50px; left: -15px; will-change: top, left;">

                                        <ul class="nav nav-tabs left" data-tabs="tabs">
                                            <li class="nav-item">
                                                <a class="nav-link " href="#dates" data-toggle="tab">
                                                    <i class="material-icons">free_cancellation</i> <?php echo $this->lang->line('menu_dates'); ?>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#profile" data-toggle="tab">
                                                    <i class="material-icons">account_circle</i> <?php echo $this->lang->line('menu_profile'); ?>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#max-table" data-toggle="tab">
                                                    <i class="material-icons">table_chart</i> <?php echo $this->lang->line('menu_max_table'); ?>
                                                </a>
                                            </li>
                    
                                            <li class="nav-item">
                                                <a class="nav-link" href="" data-toggle="modal" data-target="#TopMonthModal">
                                                    <i class="material-icons">format_list_bulleted</i> <?php echo $this->lang->line('menu_top_month'); ?>
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="logout">
                                                    <i class="material-icons">logout</i> <?php echo $this->lang->line('menu_logout'); ?>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>

                </div>
                <!-- End Navbar -->