<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('header');
?>
<div class="row text-center">
    <div class="col-md-12 login-button-container">
        <a href="<?= $authURL ?? 0; ?>">
            <button class="login-button btn btn-social btn-fill btn-facebook">
                <i class="fa fa-facebook"></i><?= $this->lang->line('login_with_facebook'); ?>
            </button>
        </a>
        <br>

        <a href="<?= $loginURL ?? 0; ?>">
            <button class="login-button btn btn-social btn-fill btn-google">
                <i class="fa fa-google"></i><?= $this->lang->line('login_with_google'); ?>
            </button>
        </a>
        <br>
        <a href="#card-login-modal" data-toggle="modal" data-target="#card-login-modal">
            <button class="login-button btn btn-social btn-fill btn-card-login">
                <i class="fa fa-credit-card"></i><?= $this->lang->line('login_with_card'); ?>
            </button>
        </a>
        <div class="modal fade" id="card-login-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">

                <div class="card card-chart">

                    <div class="btn-card-login card-header card-header-success">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4><?= $this->lang->line('login_with_card'); ?></h4>

                    </div>
                    <form action="/user_authentication" method="post" class="card-login-form">
                        <div class="card-body">
                            <input name="card_number" type="number" class="form-control" placeholder="<?= $this->lang->line('card_number'); ?>">
                        </div>
                        <div class="card-footer">
                            <input type="submit" class="btn btn-card-login" value="<?= $this->lang->line('login_button'); ?>">
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $this->load->view('footer'); ?>