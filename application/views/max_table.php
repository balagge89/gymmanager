<div class="tab-pane" id="max-table">
    <table class="table">
        <thead>
            <tr>
                <th><?= $this->lang->line('max_table_name'); ?></th>
                <th>Clean & Jerk</th>
                <th>Snatch</th>
                <th>Overhead Squat</th>
                <th>Deadlift</th>
                <th>Back Squat</th>
                <th>Front Squat</th>
                <th>Shoulder Press</th>
                <th>Squat Clean</th>
                <th>Thruster</th>
                <th>Push Press</th>
                <th>Bear Complex</th>
                <th>100 Burpee</th>
                <th>Double Under</th>
                <th>Run 5K</th>
                <th>Handstand Walk</th>
                <th>Body Weight</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($max_table as $max_table_data) { ?>
                <tr>
                    <td><?=  $max_table_data->name; ?></td>
                    <td><?=  $max_table_data->clean_jerk; ?></td>
                    <td><?=  $max_table_data->snatch; ?></td>
                    <td><?=  $max_table_data->ohs; ?></td>
                    <td><?=  $max_table_data->deadlift; ?></td>
                    <td><?=  $max_table_data->back_squat; ?></td>
                    <td><?=  $max_table_data->front_squat; ?></td>
                    <td><?=  $max_table_data->shoulder_press; ?></td>
                    <td><?=  $max_table_data->squat_clean; ?></td>
                    <td><?=  $max_table_data->thruster; ?></td>
                    <td><?=  $max_table_data->push_press; ?></td>
                    <td><?=  $max_table_data->bear_complex; ?></td>
                    <td><?=  $max_table_data->burpee; ?></td>
                    <td><?=  $max_table_data->double_under; ?></td>
                    <td><?=  $max_table_data->run_5k; ?></td>
                    <td><?=  $max_table_data->handstand_walk; ?></td>
                    <td><?=  $max_table_data->weight; ?></td>

                </tr>
            <?php } ?>
        </tbody>
    </table>
</div>