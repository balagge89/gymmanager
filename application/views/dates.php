<div class="tab-pane active show" id="dates">

    <div class="row">
        <div class="col-lg-1-5 col-md-4 col-sm-6">
            <h2><?= $events_data[0]->day; ?></h2>
            <?php
            $i = 1;
            foreach ($events_data as $key => $event_data) {

                //New weekday check
                if (isset($prev_day)) {
                    if ($prev_day != $event_data->day) { ?>
        </div>
        <div class="col-lg-1-5 col-md-4 col-sm-6">
            <h2><?= $event_data->day; ?></h2>
    <?php }
                } ?>

    <div class="card card-chart">
        <div class="card-header card-header-success  color-<?= $event_data->color; ?>">
            <h4><?= $event_data->category ?> - <?= substr($event_data->date, 10, -3); ?></h4>
            <span><?= $event_data->day; ?> - <?= substr($event_data->date, 0, -8);  ?></span>

        </div>
        <!-- Check if user has a ticket -->
        <?php if ($user_data[0]->ticket_number > 0) { ?>
            <div class="card-body">
                <a href="#">
                    <h4 class="card-title text-center" data-toggle="modal" data-target="#exampleModal-<?= $event_data->id; ?>"><?= $this->lang->line('date_attendees'); ?><span class="attendee_number"> <?= $events_attendees_list[$event_data->id]['attendees']; ?></span></h4>
                </a>
            </div>
        <?php } ?>

        <div class="card-footer">
             <!-- User Event Reg Check -->
            <?php
                $reg_deadline = date("Y-m-d H:i:s", time() + ($settings[0]->global_reg_limit * 60));
                $un_reg_deadline = date("Y-m-d H:i:s", time() + (60 * ($event_data->deadline)));
                $min_attendees = $settings[0]->min_attendees;

                $reg_check = array_search($user_data[0]->fb_id, array_column($events_attendees_list[$event_data->id]['attendees_list'], 'fb_id'));
                include('reg_button.php');
            ?>
        </div>
    </div>
    <?php $prev_day = $event_data->day; ?>

    <?php if ($user_data[0]->ticket_number > 0) { ?>
        <!-- Modal -->
        <div class="modal fade" id="exampleModal-<?= $event_data->id; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">

                <div class="card card-chart">

                    <div class="color-<?= $event_data->color; ?> card-header card-header-success">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4><?= $event_data->category ?> - <?= substr($event_data->date, 10, -3); ?></h4>
                        <span><?= $event_data->day ?> - <?= substr($event_data->date, 0, -8); ?></span>

                    </div>
                    <div class="card-body">
                        <?php foreach ($events_attendees_list[$event_data->id]['attendees_list'] as $events_attendees_data) {  ?>
                            <h4 class="card-title text-center attendee" data-toggle="modal" data-target="#exampleModal">
                                <img class="attendee_image" src="//graph.facebook.com/<?= $events_attendees_data->fb_id ?? ''; ?>/picture"><?= $events_attendees_data->name ?? ''; ?>
                            </h4>
                        <?php }  ?>
                    </div>
                    <div class="card-footer">
                        <?php include('reg_button.php'); ?>
                    </div>

                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>
        </div>
    </div>

</div>