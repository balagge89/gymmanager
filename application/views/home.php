<?php
defined('BASEPATH') or exit('No direct script access allowed');
$this->load->view('header'); ?>

<div class="wrapper ">

  <div class="ps-container ps-theme-default ps-active-y" data-ps-id="df0d3856-a721-c894-b4e3-4e9426ea957a">

    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <div class="card">

              <?php $this->load->view('nav'); ?>

              <div class="card-body">
                <div class="tab-content">
                  <?php $this->load->view('dates'); ?>
                  <?php $this->load->view('profile'); ?>
                  <?php $this->load->view('max_table'); ?>
                  <?php $this->load->view('top_month'); ?>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

<?php 
$this->load->view('footer'); 
$this->load->view('front_notifications'); 
?>