<!-- Modal -->
<div class="modal fade" id="TopMonthModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">

                <div class="card card-chart">

                    <div class="menu-header card-header card-header-success">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4><?php echo $this->lang->line('menu_top_month'); ?></h4>
                        <span>Ebben a hónapban a legtöbb edzésen résztvevők</span>

                    </div>
                    <div class="card-body">
                        <?php 
                        foreach ($top_month as $top_month_person) {  ?>
                            <h4 class="card-title text-center attendee" data-toggle="modal" data-target="#exampleModal">
                                <?= "<img class='attendee_image' src='//graph.facebook.com/".$top_month_person->fb_id."/picture'>" ?? ''; ?><?= $top_month_person->name ?? ''; ?><span class="checkin_number"> <?= $top_month_person->checkins ?? ''; ?></span>
                            </h4>
                        <?php }  ?>
                    </div>
                    <div class="card-footer">
                    </div>

                </div>
            </div>
        </div>